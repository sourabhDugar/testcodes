#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char * 
all_but_i (char * in, int i, int len) {
    char * res = malloc(len);
    memset(res, 0, len);
    char * iter = res;

    int j = 0;
    for (;j < len; j++) {
        if (i != j) {
            *(iter++) = in[j];
        }
    }
    return res;
}

char ** 
multiply_strings (char first, char ** rest, int rest_count, int rest_size) 
{
    char ** res = malloc(rest_count * sizeof(char **));
    int i = 0;
    for (; i < rest_count; i++) {
        *(res + i) = malloc(rest_size * sizeof(char) + 1);
        **(res + i) = first;
        strcpy((*(res + i)) + 1, *(rest + i));
        free(*(rest + i));
    }
    free(rest);
    return res;
}

char **
permute_this (char * in, int *result_count) 
{
    if (strlen(in) == 1) {
        char ** res = malloc(sizeof(char *));
        *res = malloc(sizeof(char));
        **res = in[0];
        *result_count = 1;
        return res;
    } else {
        int i = 0;
        int in_len = strlen(in);
        int res_count = 0;
        char once = 0;
        char **acc = 0;
        for (; i < in_len; i++) {
            char *in_local = all_but_i(in, i, in_len);
            char ** res = permute_this(in_local, &res_count);
            free(in_local);
            *result_count = res_count * in_len;
            if (!once) {
                acc = malloc(sizeof(char *) * (*result_count));
                once = 1;
            }
            res = multiply_strings(in[i], res, res_count, in_len - 1);
            int j = 0;
            for (;j < res_count; j++) {
                *(acc + i * res_count + j) = *(res + j);
            }
        }
        return acc;
    }
    return NULL;
}

void 
test_all_but_i () {
    char * res1 = all_but_i("ab", 0, 2);
    char * res2 = all_but_i("ab", 1, 2);
    char * res3 = all_but_i("abcd", 2, 4);
    char * res4 = all_but_i("abcd", 3, 4);
    printf("The various results are, res1 : %s, res2 : %s, res3 : %s\n",
            res1, res2, res3);
}

char *
swap_i_with_last(char * in, int i, int len, int restore)
{
#ifndef WITH_COPY
    char * copy = in;
    if (restore) {
        swap_i_with_last(in, i - 1, len, 0);
    }
#else
    char *copy = malloc(sizeof(char) * len);
    strcpy(copy, in);
#endif
    //printf("swapping %c with %c for %s\t", copy[i], copy[len - 1], copy);
    if (i != len - 1) {
        copy[i] = copy[i] ^ copy[len - 1];
        copy[len - 1] = copy[i] ^ copy[len - 1];
        copy[i] = copy[i] ^ copy[len - 1];
    }
    return copy;
}

void
permute_by_swapping(char * in, char * suffix, int in_len, int * count) {
    int i = 0;
    if (in_len == 1) {
        fprintf(stderr, "%s\n", in);
        (*count)++;
        return;
    }
    for (;i < in_len; i++) {
        char * swapped = swap_i_with_last(in, i, in_len, i);
        permute_by_swapping(swapped, &swapped[in_len - 1], in_len - 1, count);
#ifdef WITH_COPY
        free(swapped);
#endif
    } 
}

int
main (int argc, char ** argv) {
    //test_all_but_i();
    char *str[2] = {"abd", "bad"};
    //char ** res = multiply_strings('c', str, 2, 3);
    int i = 0;
    /*for (; i < 2; i++) {
        printf("string %s\n", *(res + i));
    }*/
    //int result_count = 0;
    //char ** res2 = permute_this(argv[1], &result_count);
    //for (i = 0; i < result_count; i++) {
    //    printf("%s\n", *(res2 + i));
    //}
    int count = 0;
    permute_by_swapping(argv[1], "", strlen(argv[1]), &count);
    printf("total count is %d\n", count);
}
