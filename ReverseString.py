#!/opt/local/bin/python2.7
import sys

def ReverseStr (str_):
    if len(str_) == 1: #basecase
        return str_
    return (ReverseStr(str_[len(str_)/2:]) + ReverseStr(str_[0:(len(str_)/2)]))
                      #^divide                          ^divide
            #^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                      # conquer/reassemble
print ReverseStr(sys.argv[1])
