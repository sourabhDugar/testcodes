#!/usr/bin/env python-2.7.1
import time
import pdb
import getpass as gp
import sys
sys.path[1:1] = ['/users/sdugar/tmp/sandbox/lib/python2.7/site-packages']
import requests as rq
import readline
import re
import signal
import subprocess as sp

#Set to 1 if debug o/p is needed
debug = 0
log = 1

password = gp.getpass('Please input your ads password>')
query_id_string = raw_input('Please enter list of queries separated by commas>')
#query_id = raw_input('Please enter list of queries separated by commas>')
user = gp.getuser()
upd_interval=3600
print "The script will run in a loop updating every every " + str(upd_interval) + " seconds"

tst = str(time.time())
tst = tst.replace('.', '')

def modUpdTime(sig, data):
    global upd_interval
    upd_interval_re = re.compile('.*upd_interval\s+:\s+(\d+).*')
    if sig == signal.SIGHUP:
        #if hangup rcvd double the upd_time
        upd_interval = upd_interval * 2
    if sig == signal.SIGINT:
        #if interrupt rcvd half the upd time
        upd_interval = upd_interval / 2
        #upd_interval = int(raw_input('enter new update time in seconds>'))
        with open('update.spec', 'r') as updspec:
            l = updspec.read()
            upd_int_line = upd_interval_re.search(l)
            upd_interval = int(upd_int_line.group(1))
        if debug:
            print 'new upd_interval : %d'%upd_interval
        if upd_interval == 0:
        #if upd_interval becomes zero exit out...
            sys.exit(1)

signal.signal(signal.SIGINT, modUpdTime)
signal.signal(signal.SIGHUP, modUpdTime)

class UpdateCoreObj():
    user = ''
    query_id = ''
    password = ''
    #Regex to figure out Auth Failure
    auth_failed = re.compile("^.*Authentication.*[F|f]ailed.*>(.*).<", re.MULTILINE)
    max_auth_retries = 5
    def __init__(self, query_id_t):
        self.user = globals()['user']
        self.password = globals()['password']
        self.query_id = query_id_t
    def UpdateCore(self):
        self.password = globals()['password']
        while True:
            #Authentication loop....
            if debug:
                print "starting auth loop for %s"%self.query_id
            try:
                while True:
                    r = rq.get('http://wwwin-ottawa.cisco.com/tfoggoa/Scrubber/showquery.html?query=' + self.query_id,
                               auth=(self.user, self.password))
                    #r = rq.get('http://wwwin-ottawa.cisco.com/tfoggoa/Scrubber/welcome.pl?r=1407978464704',
                    #            auth=(self.user, self.password))
                    if debug:
                        print "*_" * 50
                        print 'Status', r.status_code
                        print 'Headers', r.headers
                        print 'Text', r.text
                        print "*_" * 50
                        print "Query 2"
                        print "*_" * 50
                        pdb.set_trace()

                    auth_fail_str = self.auth_failed.search(r.text)
                    if auth_fail_str != None:
                        if not self.max_auth_retries:
                            print "Max Auth retries exceeded...exiting..."
                            sys.exit(1)
                        self.max_auth_retries = self.max_auth_retries - 1
                        if len(auth_fail_str.groups()) > 0:
                            print auth_fail_str.group(1)
                        else:
                            print "Error in authentication"
                        self.password = gp.getpass('Please input your ads password again>')
                        global password
                        globals()['password'] = self.password
                    else:
                        break

                payload = {'r' : tst}
                base_url = 'http://wwwin-ottawa.cisco.com/tfoggoa/Scrubber/data/' + self.query_id + '.xml'
                r = rq.get(base_url, params=payload, auth=(self.user, self.password))

                if debug:
                    print 'Url', r.url
                    print "*_" * 50
                    with open('Scrubber.txt', 'w+') as S:
                        S.write(r.text)
                import json
                base_url2 = 'http://wwwin-ottawa.cisco.com/tfoggoa/Scrubber/showupdate.pl'
                #base_url2='http://www.google.com'
                #mh = {'Request Payload' : 'ssreenat-64'}

                r = rq.post(base_url2, params=payload, data=self.query_id, auth=(self.user, self.password))
                if debug:
                    print "*_" * 50
                    print "Query 3"
                    print "*_" * 50
                    print "Url", r.url
                    print "*_" * 50
                    pdb.set_trace()
                if log:
                    with open('Scrubber.txt', 'a') as S:
                        S.write(r.text)
                    #sys.exit(1)
                    return
                return
            except Exception, e:
                print e
                continue

def UpdateMain():
    U = []
    query_id_list=query_id_string.split(',')
    for query_id in query_id_list:
        U.append(UpdateCoreObj(query_id))
    if debug:
        pdb.set_trace()
    while True:
        for u in U:
            u.UpdateCore()
        print "sleeping for %d"%upd_interval
        time.sleep(upd_interval)

UpdateMain()
