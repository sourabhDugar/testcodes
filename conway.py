#!/opt/local/bin/python2.7
import os
import time
import pyglet
import sys

def PrintRed(st):
	print "\x1B[031m%s\x1B[0m"%st

def PrintGrn(st):
	print "\x1B[031m%s\x1B[0m"%st

def CreateTestMap1():
	r = range(0, 11)
	c = range(0, 11)
	M = [[0 for x in r] for y in c]
	for re in range(0, 11):
		for ce in range(0, 11):
		#	M[re][ce] = (1 if ((re < 2 and ce == 2) or (re == 2 and ce < 2)) else 0)
			if re <= 3 and ce == 3 or re >= 10 - 3 and \
		   	   ce == 7 or re <= 3 and ce == 7 or re >= 7 and ce == 3:
				M[re][ce] = 1
			if re == 3 and ce <= 3 or re == 10 - 3 and \
			   ce >= 10 - 3 or re == 3 and ce >= 7 or re == 7 and ce <= 3:
				M[re][ce] = 1
	return M

def CreateImplodeMap():
	r = range(0, 11)
	c = range(0, 11)
	M = [[0 for x in r] for y in c]
	#Quadrant condition, should be able to write for one quad and replicate four times
	for re in r:
		for ce in c:
			if re >= 3 and re <= 10 - 3 and (ce == 3 or ce == 10 - 3):
				M[re][ce] = 1
			if ce >= 3 and ce <= 10 - 3 and (re == 3 or re == 10 - 3):
				M[re][ce] = 1
	return M

def DrawLineMapBased(M):
	outstr = ''
	for i in range(0, len(M)):
		outstr += ('\x1B[031m' if M[i] == 1 else '\x1B[034m') + str(M[i]) + '  '
	return outstr + '\n'

def DrawRow(m):
	outstr = ''
	m = (0 if m == 1 else 1)
	for i in range(1, 51):
		outstr += ('\x1B[031m' if m == 0 else '\x1B[034m') + 'a'
		m = (0 if m == 1 else 1)
		if i == 50:
			outstr += '\n'
	return outstr

def Animate():
	m = 0
	while True:
		outstr = ''
		m = (0 if m == 1 else 1)
		for i in range(0, 10):
			m = (0 if m == 1 else 1)
			outstr += DrawRow(m)
		os.system('clear')
		sys.stdout.write(outstr)
		sys.stdout.flush()
		time.sleep(0.1)

def printMap(M):
	for i in range(0, 11):
		for l in M[i]:
			print l,
		print

def AnimateMapBased(Frames):
	M1 = CreateTestMap1()
	M2 = CreateImplodeMap()
	Ml = [M1, M2]
	outstr = ''
	M = Ml[0]
	while True:
		outstr = ''
		M = (Ml[1] if M == Ml[0] else Ml[0])
		for i in range(0, 11):
			outstr += DrawLineMapBased(M[i])
		os.system('clear')
		sys.stdout.write(outstr)
		sys.stdout.flush()
		time.sleep(0.2)

AnimateMapBased('F')


