#include <iostream>
#include "graphheader.h"
#include <set>
using namespace std;

#define C 5

class Iter : public sourabh::Iter {
    public:
        set<int> adjacentOranges;
        char rotten = 0;
        Iter(int ** a) : adj(a) {}
        int **adj;
        void operator()(int i, int j) {
            adjacentOranges.insert(i);
            adjacentOranges.insert(j);
            if (adj[i][j] == 2) {
                rotten = 1;
            }
        }
};

class IterBegin : public sourabh::IterBegin {
    public:
        Iter * orangeState;
        int rottenCount = 0;
        IterBegin(Iter * state) : orangeState(state) {}
        void operator()(int i) {
            if (orangeState->rotten) {
                rottenCount += orangeState->adjacentOranges.size();
            }
            orangeState->rotten = 0;
            orangeState->adjacentOranges.clear();
        }
};

class IterBegin2 : public sourabh::IterBegin {
    public:
        void operator()(int i) {
            cout << endl << "Begin traversal from " << i << endl;
        }
        IterBegin2() {
            cout << endl << "IterBegin2::IterBegin2 " << endl;
        }
        ~IterBegin2() {
             cout << endl << "The destructor has been called for IterBegin2" << endl;
        }
};

int
main ()
{
    int arr[][C] = { {2, 1, 0, 2, 1},
                     {1, 0, 1, 2, 1},
                     {1, 0, 0, 2, 1}};

    int arr2[][C] = { {2, 1, 0, 2, 1},
                     {0, 0, 1, 2, 1},
                     {1, 0, 0, 2, 1}};

    sourabh::Graph g(15);

    for (int i = 0; i < 3; ++i) {
        for (int j = 0; j < C; ++j) {
                int edge = i * C + (j);
                if (arr2[i][j] == 0) continue;
                if (j < C - 1 && arr2[i][j + 1] > 0)
                    g.addEdge(edge, (i * C) + (j + 1));
                if (j > 0 && arr2[i][j - 1] > 0)
                    g.addEdge(edge, (i * C) + j - 1);
                if (i > 0 && arr2[i - 1][j] > 0)
                    g.addEdge(edge, ((i - 1) * C) + j);
                if (i < 2 && arr2[i + 1][j] > 0)
                    g.addEdge(edge, ((i + 1) * C) + j);
        }
    }
                
    std::cout << "Following is Depth First Traversal\n";
    // pass a custom destructor
    auto del = [](sourabh::IterBegin *p) { delete static_cast<IterBegin2 *>(p); return 0; };
    std::unique_ptr<IterBegin2, decltype(del)> b(new IterBegin2(), del);
    if (b.get() != NULL) {
        cout << "unique ptr still has a non null ref" << endl;
    } else {
        cout << "unique ptr reference is null " << endl;
    }
    g.DFS(std::move(b), NULL);
    cout << endl;
    if (b.get() != NULL) {
        cout << "unique ptr still has a non null ref" << endl;
    } else {
        cout << "unique ptr reference is null " << endl;
    }
    std::cout << std::endl;
    return 0;
}
