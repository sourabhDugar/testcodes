#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRING_Y "sat"
#define STRING_X "sun"

#define MIN(A, B)  ((A) <  (B)) ? (A) : (B)

/**
 * Edit distance problem with memoization
 *
 * source (much of it) : http://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/
 * I mostly just added the memoization part, do not know how effective it is!
 */

int 
edit_distance_recursive (char *x, char *y, int m, int n, int *memo) {
    if (m == 0 && n == 0) {
        memo[0] = 0;
        return 0;
    } else if (m == 0) {
        memo[n] = n;
        return n;
    } else if (n == 0) {
        memo[(strlen(y) + 1) * (m)] = m;
        return m;
    }
    int cost1, cost2, cost3;

    cost1 = memo[((strlen(y) + 1) * (m - 1)) + (n)] == -1 ? edit_distance_recursive(x, y, m - 1, n, memo) :
        memo[((strlen(y) + 1) * (m - 1)) + (n)];
    ++cost1;
    cost2 = memo[((strlen(y) + 1) * (m)) + (n - 1)] == -1 ? edit_distance_recursive(x, y, m, n - 1, memo) :
        memo[((strlen(y) + 1) * (m)) + (n - 1)]; 
    ++cost2;
    cost3 = memo[((strlen(y) + 1) * (m - 1)) + (n - 1)] == -1 ? edit_distance_recursive(x, y, m - 1, n - 1, memo) :
        memo[((strlen(y) + 1) * (m - 1)) + (n - 1)];
    cost3 += (1 * (x[m - 1] != y[n - 1]));
    memo[((strlen(y) + 1) * (m)) + (n)] = MIN(MIN(cost1, cost2), cost3);  
    return MIN(MIN(cost1, cost2), cost3);    
}

int
main (int c, char **argv) {
    char *string1 = argv[1];
    char *string2 = argv[2];
    int *memo = (int *)malloc( ( strlen(string1) + 1 ) * ( strlen(string2) + 1 ) * sizeof(int) );
    int i = 0;
    for (i = 0; i < (strlen(string1) + 1) * (strlen(string2) + 1); i++) {
        memo[i] = -1;
    }
    int edit_cost = edit_distance_recursive(string1, string2, strlen(string1),
            strlen(string2), memo);
    // print the DP table, to help debug and understand the solution
    for (i = 0; i < (strlen(string1) + 1) * (strlen(string2) + 1); i++) {
        if (i % (strlen(string2) + 1)  ==  0 && i != 0) {
           printf("\n");
        } 
        printf("%d ", *(memo + i));
    }
    printf("\n");
    printf("The minimum edit cost to convert %s from %s is %d\n",
            string1, string2, edit_cost);
}
