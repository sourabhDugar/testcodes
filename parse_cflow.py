#!/usr/bin/env python
import string
import re
import subprocess as sp
import pdb

class Parser():
    #Init
    level_indicator='-'
    top_level={}
    stack=[]
    dict_stack=[]
    dash=0
    curr_level=top_level
    curr_label=''
    pattern=level_indicator+'{'+str(dash + 1)+'}(.*):(.*)\n'

    def push_label_on_stack (self,label):
        if self.dash <= len(self.stack):
            self.stack[self.dash-1]=label
            self.dict_stack[self.dash-1]=self.curr_level
        else:
            self.stack.append(label)
            self.dict_stack.append(self.curr_level)

    def change_level (self,level='+',line=''):
        if level == '+':
            #expect the next level
            self.dash+=1
        else:
            #reset current dictionary
            self.curr_level=self.top_level
            #Get the current level
            self.dash=level
            #set current level to correct level based on
            #label stack
            #for i in range(0, self.dash-1):
            #    self.curr_level=self.curr_level[self.stack[i]]
            if self.dash > 1:
                self.curr_level=self.dict_stack[self.dash-2]
            else:
                self.curr_level=self.top_level
            #update pattern to account for new dash count
            self.pattern=self.level_indicator+'{'+str(self.dash)+'}(.*):(.*)\n'
            #search the current line to obtain the new function name
            # (or label)
            res=re.search(self.pattern,line)
            self.curr_label=res.group(1)
        self.curr_level[self.curr_label]={}
        self.curr_level=self.curr_level[self.curr_label]

    def get_current_level (self,line):
        pattern_d_cnt='(-{1,})'
        d_match=re.search(pattern_d_cnt,line)
        d_cnt=len(d_match.group(1))
        return (d_cnt)

    def parse_cflow (self):
        gp=open('e2cGraph', 'r')
        line=gp.readline()
        while line != '':
            #pdb.set_trace()
            #update pattern to account for new dash count
            self.pattern=self.level_indicator+\
                         '{'+str(self.dash + 1)+'}(.*):(.*)\n'
            res=re.search(self.pattern,line)
            if res != None:
                self.curr_label=res.group(1)
                self.change_level()
                #add a new label
            else:
                new_dash=self.get_current_level(line)
                #pdb.set_trace()
                self.change_level(new_dash,line) 
                #replace an existing label
            self.push_label_on_stack(self.curr_label)
            #pdb.set_trace()
            line=gp.readline()

pI=Parser()
pI.parse_cflow()
pdb.set_trace()
print pI.top_level
