#!/opt/local/bin/python2.7
import pdb
class Foo:

    def foo(self):
        return True

    def foo_override(self):
        return False

    def call_foo(self):
        return self.foo()

    def __init__(self, override=False):
        if override:
            self.foo = self.foo_override

def FooMooUt():
    #This is ut
    m = Foo(True)
    f = Foo()
    print"expect True: f.foo() %s"% f.foo()
    print"expect False: m.foo() %s"% m.foo()
    print"expect True: Foo.foo(f) %s"% Foo.foo(f)
    #Here is the surprise the call using the class identifier with object as m, doesn't
    #return False, intead it just calls foo, the call to foo using call_foo however will
    #give expected results (or take into account object passed
    print"expect False: Foo.foo(m) %s" % Foo.foo(m)
    print "expect True: Foo.call_foo(f) %s" % Foo.call_foo(f)
    print "expect False: Foo.call_foo(m) %s" % Foo.call_foo(m)
if __name__ == '__main__':
    FooMooUt()
