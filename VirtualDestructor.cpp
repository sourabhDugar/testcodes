#include <iostream>

using namespace std;


template <typename Of>
class GetterT
{
protected:
    Of memberType;
public:
    GetterT(Of of) : memberType(of) {
        cout << "GetterT initialized" << endl;
    }
    virtual ~GetterT();
    Of getOf() const {
        return memberType;
    }
};


template <typename Of>
GetterT<Of>::~GetterT() {
    cout << "deleted GetterT" << endl;
}


template <typename Of>
class Container : public GetterT<Of>
{
public:
    Container(Of of) : GetterT<Of>(of) {
        cout << "initialized Container of: " << of <<  endl;
    }
    virtual ~Container();
    friend ostream& operator<<(ostream &st, const Container<Of> &c);  
};


template <typename Of>
Container<Of>::~Container() {
    cout << "deleted base Container" << endl;
}


//template class Container<int>;
    

template<>
class Container< Container<int> > : public GetterT< Container<int> > {
public:
    Container(int of): GetterT< Container<int> >(of) {
        cout << "Called Container::int constructor " << of << endl;
    }
    virtual ~Container() {
        cout << "deleted base Container<int>" << endl;
    }
};


ostream&
operator<<(std::ostream& strm, const Container<int> & a) {
   strm << a.getOf();
   return strm; 
}
        

class SpecialContainer : public Container< Container<int> >
{
public:
    SpecialContainer(int of) : Container(5) {
        cout << "initialized SpecialContainer" << endl;
    }
    ~SpecialContainer() {
        cout << "Deleted SpecialContainer" << endl;
    }
};


int
main (int argc, char **argv)
{
    Container< Container <int> > *container = new SpecialContainer(5);
    Container<int> *container2 = new Container<int>(5);
    cerr << "of is " << container2->getOf() << endl;
    cerr << "of is " << container->getOf() << endl;
    delete container;   
    delete container2;

    GetterT<int> *intGetter = new GetterT<int>(5);
    cout << "Of is " << intGetter->getOf() << endl;
}
