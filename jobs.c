#include <stdio.h>
#include <stdlib.h>


// arr {10, 7, 8, 12, 6, 8}
// T(jobs, i, n, k) = k * jobs[i] + T(jobs, i+1, n, k) +
//                    max(k * jobs[i], T(jobs, i+1, n - 1, k))

#define MIN(a, b) ((a) < (b)) ? (a) : (b)
#define MAX(a, b) ((a) > (b)) ? (a) : (b)

int
minTime(int *jobs,
        int i/* starting point in the jobs array */,
        int n/* number of ppl with equal capacity available to do the job*/,
        int k/* capacity : time take to do 1 unit of work*/,
        int len/* total number of jobs or/i.e the length of the jobs array*/,
        int ini/* work already pending on the first assignee in the n passed to this function*/)
{
    if (n == 0) {
        return 900000; // some big number
    }
    if (n == 1) {
        int cost = 0;
        while (i < len) {
            cost += (jobs[i] * k);
            i++;
        }
        return cost;
    }
    if (i == len - 1) {
        return ini + (k * jobs[i]);
    }
    int c1 = (k * jobs[i]) + ini; /* assign the job to the first person in n, time adds up*/
    /* This is the case where we assign the next job to first assignee*/
    int c2 = minTime(jobs, i + 1, n, k, len, c1); /* time taken to do the rest of the jobs, where we are still assigning more work to first assignee*/
    /* This is the case when next job is given to other assignees, current assignee is no longer assigned new work*/
    int c3 = minTime(jobs, i + 1, n - 1, k, len, 0);/* time taken to do the rest of the jobs, where first assignee is no longer used, rest of the assignees get work from now on*/
    int c4 = MAX(c1, c3);/* The time taken to do the jobs would be the max of the first assignee doing the newly assigned job and any previous, and the time taken by 
    the rest of assignees to finish the jobs*/

    return MIN(c2, c4);/*The overall min time is the minimum found by assigning a job to first assignee or not assigning the first job to him*/
}

int
main (int argc, char **argv)
{
    int jobs[6] = {10, 7, 8, 12, 6, 8};
    int jobs2[3] = {4, 5, 10};
    int ans = minTime(jobs2, 0, 2, 5, 3, 0);
    printf("The answer is %d\n", ans);
}
