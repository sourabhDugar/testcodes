#include <iostream>
#include "stdlib.h"
class Y;
class Y {
public:
   static std::string getName() { return "Y"; }
};
using namespace std;
template <class c1> class X {
public:
   template<typename returnT> returnT getAThing(std::string param);
   static std::string getName();
private:
   c1 theData;
};

// This works ok...
template <class c1> std::string X<c1>::getName() {
   return c1::getName();
}
int getIntThing(std::string param) {
	return (1000);
}
// This blows up with the error:
// error: prototype for 'int X<c1>::getAThing(std::string)' does not match any in class 'X<c1>'
template <> template <> int X<Y>::getAThing(std::string param) {
   return getIntThing(param); // Some function that crunches on param and returns an int.
}
template <> template <> char* X<Y>::getAThing(std::string param) {
   return "carate"; // Some function that crunches on param and returns an int.
}

// More specialized definitions of getAThing() for other types/classes go here...

/*
template<>
int X<Y>::getAThing(std::string param) {
	cout << "Specialized declaration" << endl;
	return 1000;
}
*/
int main(int argc, char* argv[])
{
   X<Y> tester;
   char *anIntThing = tester.getAThing<char *>(std::string("param"));
   cout << "Name: " <<  tester.getName() << endl;
   cout << "An int thing: " << anIntThing << endl;
}
