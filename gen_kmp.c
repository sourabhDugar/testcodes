#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int argc, char **argv)
{
    char *xyz = "string";
    long long byz = xyz;

    int   prev_match_len          = 0;
    char *in = argv[1], *in_copy  = argv[1];
    int   in_len                  = strlen(in);
    int  *kmp_table               = (int *)malloc((in_len + 1) * sizeof(int));
    int   j                       = 0;
    kmp_table[0] = -1;
    kmp_table++;

    while (j < in_len) {
        if (in_copy[prev_match_len] == in[j] &&
            /*
             *  To ensure that this is a suffix that is also a prefix,
             *  otherwise we might be matching the prefix with itself
             */
            j != prev_match_len) {
            kmp_table[j] = ++prev_match_len;
        } else if (in_copy[0] == in[j] && j != prev_match_len) {
            kmp_table[j] = (prev_match_len = 1);
        } else {
            kmp_table[j] = (prev_match_len = 0);
        }
        printf("in[%d] : %c, prev_match_len : %d\n", j,
                in[j], prev_match_len);
        j++;
    }
    kmp_table--;

    int i = 0;
    for (; i <= in_len; i++) {
        printf("%d, ", kmp_table[i]);
    }
    printf("\n");

    char *word = argv[2];
    int  word_len = strlen(word);
    j = 0, i = 0; //reset j
    int matched = 0;

    while (j < word_len) {
        if (word[j] == in[i]) {
            matched++;
            if (matched == in_len) {
                printf("Found substring at %d\n", j - in_len + 1);
                return 0;
            }
            j++, i++;
        } else {
            j = matched == 0 ? j + 1 : j;
            i = kmp_table[i];
            i = i == -1 ? 0 : i;    
            matched = i;
        }
    }
    printf("substring %s not found in %s\n", in, word);
}
