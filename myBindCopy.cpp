#include <iostream>
#include <string>

using namespace std;
class Widget {
public:
    Widget() : i(5) { cout << "constructor called" << endl; }
    int i;
    friend ostream& operator<<(ostream& out, Widget& w);
};

ostream& operator<<(ostream& out, Widget& w)
{
    out << w.i << endl;
    return out;
}

void DoSomething (Widget& w) {
    cout << w << endl;
}


template<int I>
class placeholder{
public:
    placeholder() { }
    //template< class T > placeholder( T const & t_) { }
};

placeholder<1> _1;
placeholder<2> _2;
placeholder<3> _3;

template<int I>
struct value_obj {
    int t_;
};

template<>
struct value_obj<1> {
    placeholder<1> t_;
};

template<>
struct value_obj<0> {
    int t_;
};
struct  base {
    base() {}
};

template <typename I>
struct value : public base {
    typedef I type;
    value (type t_) : i_(t_) { }
    type i_;
};
template <typename I>
struct type {
    typedef I stored_type;
    I i_;
    base *v;
    placeholder<1> pl_;
    int idx;
    //value_obj<idx> vob; 
    template <typename T> 
    type (T const & t_) : i_(t_), idx(2)  { ; cout << "copy construct called" << endl; }
    template <int In> 
    type (placeholder<In> const & p_) : pl_(p_), idx(0) { v = new value<placeholder<In>>(p_); cout << "placeholder arg" << endl; }
};

template < int I >
struct type< placeholder<I> > {
    typedef placeholder<I> stored_type;
};
/*
template<typename A1>
class list1
{
public:
    list1(A1 a1): a1_(a1){}
    A1 operator[](placeholder<1>) const { return a1_; }
    template<typename T>
    T operator[](T v) const { return v; }
private:
    A1 a1_;
};
*/

template<typename A1>
class list1
{
public:
    list1(A1 a1): a1_(a1){}
    A1 operator[](placeholder<1>) const { return a1_; }
    template<typename T>
    T operator[](T v) const { cout << "returning : " << v << endl; return v; }
private:
    A1 a1_;
};

//this solution works //but isn't compile time
//there is branching at run time in operator[]
//with a proper overload/specialization this can go away
template <typename T>
class list1<type<T>>
{
public:
    list1(T t_) : a1_(t_) { }
    //T operator[](type<placeholder<1>> v) const { cout << "I am called" << endl; return a1_; }
    template<typename E> 
     E operator[](type<E> const & v) const { 
        if (v.idx) { return v.i_; }
        return a1_;
    }
     T operator[](placeholder<1> pl) const {
        return a1_;
    }
     template<typename E>
     T operator[](E const & v) const {
        return v;
     }
    
    //T operator[](placeholder<1>) const { return a1_; }
private:
    typedef T stored_type;
    T a1_;
};
#if 0
template <>
class list1<type<placeholder<1>>>
{
public:
    template <typename T>
    list1(T t_) : a1_(t_) { }
    template <typename T>
    T operator[](type<placeholder<1>> v) const { cout << "I am called" << endl; return a1_; }
private:
    typedef T stored_type;
};
#endif
class list_base {
public:
       
};
template<typename A1, typename A2, typename A3>
class list3 : public list_base
{
public:
    list3(A1 a1, A2 a2, A3 a3): a1_(a1), a2_(a2),
     a3_(a3) {/* cout << a1_.i_ << "," << a2_.i_ << "," << a3_.i_ << endl;*/ }
private:
    A1 a1_;
    A2 a2_;
    A3 a3_;
public:
    template<typename F, typename List1>
    void operator()(F f, List1 list1)
    {
      cout << "in list3 ()" << endl;
      //cout << a1_ << endl;
      f(list1[a1_], list1[a2_], list1[a3_]);
    }
};
/*
template<typename T1, typename T2, typename T3>
class list3<type<T1>, type<T2>, type<T3>>
{
public:
    list3(T1 a1, T2 a2, T3 a3): a1_(a1), a2_(a2),
     a3_(a3) { cout << a1_.i_ << "," << a2_.i_ << "," << a3_.i_ << endl; }
private:
    T1 a1_;
    T2 a2_;
    T3 a3_;
public:
    template<typename F, typename List1>
    void operator()(F f, List1 list1)
    {
      cout << "in list3 ()" << endl;
      //cout << a1_ << endl;
      f(list1[a1_], list1[a2_], list1[a3_]);
    }
};
*/
class Base {
};

template<typename F, typename List>
class binder : public Base
{
private:
    List list_;
    F    *f_;

public:
    binder(F f, List list) : f_(f), list_(list) { }
  template<typename A1>
  void operator()(A1 a1)
  {
    list1<A1> list(a1); // Explanation coming
    list_(f_, list);
  }
};
//typedef std_bind_c* std_bind;
template<typename R, typename A1, typename A2, typename A3>
class binder<R(A1, A2, A3), list3<A1, A2, A3>> : public Base
{
private:
    typedef R(F)(A1, A2, A3);
    typedef list3<A1, A2, A3> List;
    List list_;
    F    *f_;

public:
  binder(F f, List list) : f_(f), list_(list) { }
  template<typename A1_o>
  void operator()(A1_o a1)
  {
    list1<A1_o> list(a1); // Explanation coming
    list_(f_, list);
  }
};

template<typename Sig>
class bind2
{

};

template<typename R, typename A1, typename A2, typename A3>
class bind2<R(A1, A2, A3)>
{
private:
    typedef R(F)(A1, A2, A3);
    typedef list3<A1, A2, A3> List;
    List list_;
    F    *f_;

public:
  bind2(F f, List list) : f_(f), list_(list) { }
  template<typename A1_o>
  void operator()(A1_o a1)
  {
    list1<A1_o> list(a1); // Explanation coming
    list_(f_, list);
  }
};
#if 0
template<typename F, typename A1, typename A2,
   typename A3>
binder<F, list3<A1, A2, A3> >/* Base* */ bind(F f, A1 a1, A2 a2,
   A3 a3) {
  typedef list3<A1, A2, A3> list_type;
  list_type  list(a1, a2, a3);
  return binder<F, list_type>(f, list);
}
#endif
#if 0
template<typename F, typename A1, typename A2,
   typename A3>
bind2<F>/* Base* */ bindx(F f, A1 a1, A2 a2,
    A3 a3) {
  typedef list3<A1, A2, A3> list_type;
  list_type  list(a1, a2, a3);
  return bind2<F>(f, list);
}
#endif
#if 0
template<typename R, typename A1, typename A2,
   typename A3>
bind2<R(A1, A2, A3)>/* Base* */ bindx(R(*f)(A1, A2, A3), A1 a1, A2 a2,
    A3 a3) {
  typedef list3<A1, A2, A3> list_type;
  list_type  list(a1, a2, a3);
  return bind2<R(A1, A2, A3)>(f, list);
}
#endif
int
test_f (int a, int b, int c)
{
    cout << "value of a + b + c " << a+b+c << endl;
    return 5;
}
namespace std {
    typedef Base* base;
};
#if 1 
template <typename Sig>
class bind_impl {
};
#endif
#if 1
template<typename R, typename A, typename B, typename C
        >
class bind_impl<R(A, B, C)> {
private:
    list3<type<A>, type<B>, type<C>> list_;
    typedef R(F)(A, B, C);
    F              *f_;
public:
    template<typename E, typename X, typename G>
    bind_impl(F fn, E e, X f, G g) : f_(fn), list_(e, f, g)  {
    }
    template <typename T>
    void operator()(T t_) {
        list1<type<T>> list(t_);
        cout << "in bind_impl ()" << endl;
        list_(f_, list);
    }
};
#endif
class bind_base {
public:
    //virtual void operator()() = 0;
    virtual void operator()(int) = 0;
    
};
template < typename F, typename L >
class bind_convert : public bind_base
{
    L l_;
    F *f_;
public:
    bind_convert(F f, L l) : f_(f), l_(l) { }
    template <typename T>
    void operator() (T t_)
    {
        list1<T> list_(t_);
        cout << "in bind_convert 2" << endl;
        l_(f_, list_);
    }
    
    void operator()(int) {
        list1<int> list_(5);
        l_(f_, list_);
    }
    
};

template<typename F, class A1, class A2, class A3>
bind_convert< F , list3<A1, A2, A3> > *
bind(F& f, A1 a1, A2 a2, A3 a3) 
{
    typedef list3<A1, A2, A3> list_type;
    return new bind_convert<F, list_type>(f, list_type(a1, a2, a3));
}
template<typename Sig>
class myfunc {
};

template<typename R, typename A1, typename A2, typename A3>
class myfunc<R(A1, A2, A3)> {
public:
    typedef R(F)(A1, A2, A3);
    bind_base *B;
    template <typename T>
    void operator()(T t) {
        (*B)(t);
    }
    template <typename X = A1, typename Y = A2, typename Z = A3>
    myfunc(bind_base * b) : B(b) { }
};

template <class T>
class type2
{
    public:
        template<typename I>
            type2(I const & i) { }
};

template <class T>
struct empty_type { };
int
main (int argc, char** argv) {
    Widget w;
    DoSomething(w);
    empty_type<Widget>();
    //placeholder<1> p_1 = w;
    //type<placeholder<1>> type_pl(_1);
    //type<type<int>> type_int = _1;
    //bind_convert< int(int, int, int), list3<int, int, placeholder<1> > > b
   /* myfunc<int(int,int,int)> b */
    //std::function<int(int,int,int)> b 
     //      = bind(test_f, 10, 5, _1);
    type2<int> bt = bind(test_f, 10, 5, _1);
    myfunc<int(int, int, int)> bt2 = static_cast<bind_base *>(bind(test_f, 10, 5, _1));
    //b(1), b(2), b(3), b(4);
    //bind_base x();
    (bt2)(55);
    //Widget w_1 = p_1;
    //_1::type x = 5;
    //cout << x << endl;
    //int(&f)(int,int,int) = test_f;
    //bind2<int (int,int,placeholder<1>)> b3 = bindx<int(int,int,placeholder<1>), int, int, placeholder<1>>(&test_f, 5, 3, _1);
    //b3(8);
    //binder<int, int, int, int> b2;
    //binder<int(int,int,int)> b1;
    //binder<int(int,int,int), list3<int,int,placeholder<1>>> b = bind<int(int,int,int),int,int,int>(&test_f, 5, 3, _1);
    //bind_convert<int(int,int,int)> b(&test_f, 10, 10, _1);
    //bind_convert< int(int, int, int) > bt = bind<int(int,int,int)>(&test_f, 10, 10, _1);
    //binder<int, int, int, placeholder<1>> b = bind<int(int,int,int),int,int,placeholder<1>>(&test_f, 5, 3, _1);
    //std_bind b = bind(test_f, 5, 3, _1);
    //b(1); b(2), b(3), b(4), b(5);
    //b(2); b(3); b(4); b(5);
    return 0;
}
