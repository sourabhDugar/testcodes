#include <iostream>
#include "graphheader.h"
using namespace std;

// Driver program to test methods of graph class
int main(int argc, char ** argv)
{
    //int * arr = new int[atoi(argv[1])];
    // Let us create a disconnected graph without
    // edges 0-2 and 0-3 in above graph
    sourabh::Graph g(5);  // Total 5 vertices in graph
    g.addEdge(1, 0);
    g.addEdge(2, 1);
    g.addEdge(3, 4);
    g.addEdge(4, 0);
 
    std::cout << "Following is Depth First Traversal\n";
    g.DFS();
    std::cout << std::endl;
    return 0;
}
