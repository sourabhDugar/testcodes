#include <iostream>
#include "stdlib.h"
#include <string>
using namespace std;

struct Base{
   char *name;
   Base(char *n) : name(strdup(n)) { cout << "Base ctor caller\n"; }
   Base& operator= (const Base& other) {
    free (name);
    name = strdup (other.name);
    cout << "Base assignment called\n";
   }
 };
 struct bmain {
 	char *name;
	bmain(char *n) : name(strdup(n)) { cout << "Called before meain " << name << endl;}
 };

 struct A: virtual Base{
   int val;
   A():Base((char *)"A"){ cout << "Class A ctor called\n"; }
 };

 struct B:virtual Base{
   int bval;
   B():Base((char *)"B"){ cout << "Class B ctor called\n"; }
 };

 struct Derived:public A, public B{
   string s = string("Derived");
   Derived(): Base((char *)s.c_str()) { cout << "Class Derived ctor called\n"; }
 };

 void func(Derived &d1, Derived &d2)
 {
   cout << "Assign function call\n";
   d1 = d2;
 }
 static bmain B = bmain("omega");

int
main ()
{
   Derived d1, d2;
   string s = string("der");
   d1.A::name = strdup("Ad1");
   d2.A::name = strdup("Ad2");
   d2.B::name = strdup("Bd2");
   d2.A::name = strdup("Ad2");
   cout << "len of s.c_str() is " << strlen(s.c_str()) << endl;
   //Below two lines will show that A & B classes of Derived share the
   //same base, due to virtual inheritance, i.e. d2.B::name == d2.A::name
   //Both will be equal to whatever was assigned last, in this case it is
   //d2.A::name.
   cout << "d2 B::name is " << d2.B::name << endl;
   cout << "d2 A::name is " << d2.A::name << endl;
   func(d1, d2);
}
