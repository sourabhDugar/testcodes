#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
main (int c, char **argv) 
{
    char *input = argv[1];
    int required_fbs = atoi(argv[2]);
    printf("The input is %s, require fbs are %d\n", input, required_fbs);

    int in_len = strlen(input);
    int i = 0;
    char prev_occ = 0;
    char curr_occ = 0;
    char fbs = 0;
    char done = 0;

    for (; i < in_len; i++) {
        curr_occ = input[i] - '0';
        
        // correct the count
        if (curr_occ == 1 && prev_occ == 1)
            if (fbs > 0)
                fbs--;
        
        if (fbs == required_fbs)
            done = 1;

        if (curr_occ == 0 && prev_occ == 0) {
            fbs++;
            // we are planting a flower here..
            prev_occ = 1;
        } else {
            // state of the current position for the next iteration
            prev_occ = curr_occ;
        }
        
        if (fbs == required_fbs && done == 1)
            break;
    }

    if (fbs == required_fbs)
        printf("It is possible to plant the required number of testbeds\n");
    else
        printf("It is not possible to plant the desired number of flowers on this flower bed\nSorry :P !\n");
}
