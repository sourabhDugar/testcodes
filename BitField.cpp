#include <iostream>

using namespace std;

namespace sourabh {
class Bits {
private:
    int bitfield;
    static const int signed_bit_set = 0x80000000;
    static const unsigned int signed_bit_unset = ~0x80000000;
public:
    Bits() : bitfield(0) { }
    Bits(const int n) : bitfield(n) { }
    int first_unset_bit();
    friend ostream & operator<<(ostream & oss, Bits& bits);
};
 
int   
Bits::first_unset_bit()
{
    int inverted = ~bitfield;
    inverted &= (bitfield + 1);
    
    int unset_bit_pos = 0;
    if (inverted < 0) {
        return 32;
    } else if (inverted > 0) {
        while (inverted > 0) {
            unset_bit_pos++;
            inverted >>= 1;
        }
    }
    return unset_bit_pos;
}

ostream &
operator<<(ostream & oss, Bits& bits)
{
    unsigned int mask = 0xF0000000;
    int shift = 28;
    int field = bits.bitfield;
#ifdef DEBUG
    cout << "signed_bit_set:" << (unsigned int)Bits::signed_bit_set <<
            ", signed_bit_unset:"<< Bits::signed_bit_unset << endl;
#endif
    oss << hex << uppercase;
    for (; mask > 0; mask >>= 4, shift -= 4) {
        oss << ((field & mask) >> shift) << " ";
    }
    return (oss << dec << nouppercase);
} 
};

int
main ()
{
    for (int i = 0, val = 0; i <= 32; ++i, (val <<= 1)++) {
        sourabh::Bits * bits = new sourabh::Bits(val);
        cout << *bits << ": ";
        cout << "The first unset bit is " <<  bits->first_unset_bit() << endl;
    } 
    return 0;
}
