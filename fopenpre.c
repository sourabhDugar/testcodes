#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include <dlfcn.h>
/*
 *Example of dlopen and dlsym
 *
 */
FILE *log_file = 0;
typedef FILE *(*fopen_td)(const char *filename, const char *perm);
fopen_td original_fopen = NULL;

FILE *
fopen(const char *filename, const char *perm)
{
	FILE *retfile = 0;
	original_fopen = dlsym(RTLD_NEXT, "fopen");
	if (!original_fopen) {
		printf("Failed to find library fopen function\n");
		return (NULL);
	}
	if (access("access_log", R_OK) == -1) {
		// open log file
		log_file = original_fopen("access_log", "wa+");
		if (!log_file)
			return (NULL);
	} else {
		fprintf(stdout, "In else\n");
		log_file = original_fopen("access_log", "a+");
		if (!log_file)
			return (NULL);
	}
	retfile = (original_fopen(filename, perm));
	if (!retfile) return (NULL);
	time_t rawtime;
	struct tm * timeinfo;
	time ( &rawtime );
	timeinfo = localtime ( &rawtime );
	fprintf(log_file, "File %s accessed at %s", filename, asctime(timeinfo));
	return (retfile);
}
#if 0
int
main (int argc, char *argv[])
{
	FILE *testfile = 0;
	testfile = fopen(argv[1], "wa+");
	if (testfile)
		printf("openend file %s successfully\n", argv[1]);
	fclose(testfile);
}
#endif


