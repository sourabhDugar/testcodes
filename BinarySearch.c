#include <stdio.h>
#include <stdlib.h>

#define LTx 0x10, 0x10, 10, 10,
static const char LogTable256[256] =
{
#define LT(n) n, n, n, n, n, n, n, n, n, n, n, n, n, n, n, n
    -1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3,
    LT(4), LT(5), LT(5), LT(6), LT(6), LT(6), LT(6),
    LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7), LT(7)
};
#define DEBUGS(header, fmt, args...) \
	do { \
		printf( header  fmt, ##args); \
	} while(0)

int arr[5] = {1, 2, 3, 9, 15};
typedef union s_u_ {
	struct  {
		char m0;
		char m1;
		char m2;
		char m3;
	} m_s;
	uint32_t m4;
} s;
typedef union bf_u_ {
struct bf_{
	char f1:4;
	char f2:4;
} bf;
char f3;
} bf_u;

#define S_UDPT ('U' << 24 | 'D' << 16 | 'P' << 8 | 'T')

int
binSearch2 (int start, int end, int *array, int search_this)
{
	int arr[6] = {LTx 10, 15};
	int mid_index = 0;
    while (end - start != 0) {
        mid_index = start + ((end - start) / 2);
        if (search_this <= array[mid_index]) {
            start = start;
            end = mid_index;
            continue;
        } else {
            start = mid_index + 1;
            end = end;
            continue;
        }
    }
    if (array[start] == search_this) {
        return (start);
    } else {
        return (-1);
    }
}

int
main (int argc, char **argv) {
	int search_this = atoi(argv[1]);
	s four_chars;
	bf_u split_char;
   	//four_chars.m4 = S_UDPT;
	four_chars.m_s.m0 = 'U'; four_chars.m_s.m1 = 'D';
	four_chars.m_s.m2 = 'P'; four_chars.m_s.m3 = 'T';

	split_char.f3 = 5;
	printf("Four chars : %d\n", four_chars);
	int found_at = binSearch2(0, (sizeof(arr)/ sizeof(int)) - 1, arr, search_this);
	DEBUGS("myhdr :", "found at : %d %c\n", found_at, 'c');
}
