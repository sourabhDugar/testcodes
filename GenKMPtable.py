#!/opt/local/bin/python2.7
import sys
import pdb

if __name__ == '__main__':
	W = sys.argv[1] + ''
T = []
T.append(-1)
T.append(0)

def ProperSuffix(substr, l):
	st = len(substr) - l
	return substr[st:]

def GenTable(i):
	global T
	if __name__ == '__main__':
		for e, t in enumerate(T):
			print '(', e,t, ')',
		print
		print '-' * 10
	#if i == 8:
	#	pdb.set_trace()
	#pdb.set_trace()
	if T[i - 1] < 0:
	#Consider suffixes of len 1
		if W[i] == W[0]:
			T.append(1)
		else:
			T.append(0)
		return
	if T[i - 1] >= 0:
		l = T[i - 1] + 1
		while l >= 0:
			suf = ProperSuffix(W[:i], l)
			if suf == W[0:l]:
				T.append(l)
				return
			else:
				l = l - 1
		if l < 0:
			T.append(0)

#Not needed....
def FixUpT():
	global T
	for i in range(0, len(T)):
		if T[i] > 0:
			T[i] = T[i] - 1
#Not needed....

def main(w):
	global T
	if __name__ != '__main__':
		global W
		W = w
	for i in range(2, len(W)):
		GenTable(i)
	#FixUpT()
	if __name__ == '__main__':
		for w in W:
			print "%2s"%w,
		print
		for t in T:
			print "%2d"%t,
		print
	else:
		return T

if __name__ == '__main__':
	main(W)
