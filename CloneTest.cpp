#include <iostream>
#include "stdio.h"
using namespace std;

class A {
private:
    int a;
    int b;
public:
    A(int x, int y) : a(x), b(y) { cout<<"const A called"<<endl;}
    int c;
    int get_int_a();
    int get_int_b(); 
};

int
A::get_int_a ()
{
    return this->a;
}
int
A::get_int_b ()
{
    return this->b;
}

class B : public A {
private:
    int B_a;
    int B_b;
public:
    B(int x, int y) : B_a(x), B_b(y), A(x+5, y+5) { cout<<"const A called"<<endl;}
    int B_c;
    int get_int_B_a();
    int get_int_B_b();
    B *clone();
};


int
B::get_int_B_a ()
{
    return this->B_a;
}
int
B::get_int_B_b ()
{
    return this->B_b;
}
B *
B::clone ()
{
    B *b_clone = (B *)malloc(sizeof(B));
    memcpy(b_clone, this, sizeof(B));
    return b_clone;
}

int
main (int argc, char **argv)
{
    A *a_inst = new A(10, 15);
    B *b_inst = new B(4, 3);
    printf("b_inst->A::a is %d\n", b_inst->A::get_int_a());
    printf("b_inst->A::b is %d\n", b_inst->A::get_int_b());
    B *copy_of_b_inst = b_inst->clone();
}
