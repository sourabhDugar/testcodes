package main

import (
	"fmt"
)

func Sequence() (func() string) {
	var cur int
	index := make([]string, 26, 26)

	for i := 0; i < cap(index); i++ {
		index[i] = string('A' + i)
	}

	return func() (ret string) {
		defer func() {
			cur++
		}()

		// convert the number into base 26
		for n := cur; n >= 0; n-- {
			ret = index[n % 26] + ret
			fmt.Println("cur is ", cur)
			n /= 26
		}

		return
	}
}

func main() {
	seq := Sequence()
	for i := 0; i < 1000; i++  {
		//if i > 6000 {
			fmt.Printf("%d, %q\n", i, seq())
		//}
	}
}