package main
import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	x, y := 0, 0
	index := 0
	return func() (ret int) {
		defer func() {
			x, y = y, ret
			index++
		}()

		if index < 2 {
			//fmt.Println("first two", index, firstTwo[index])
			ret = index
		} else {
			ret = x + y
		}
		return
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 25; i++ {
		fmt.Println(f())
	}
}
