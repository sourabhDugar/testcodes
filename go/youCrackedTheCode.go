package main

import (
	"io"
	"os"
	"strings"
	"fmt"
	"math"
)

type rot13Reader struct {
	r io.Reader
}

func (r rot13Reader) Read(b []byte) (int, error) {
	//fmt.Println('a')
	for {
		l := len(b)
		c := cap(b)

		if l < c {
			c = l
		}

		z := make([]byte, c, c)
		count, err := r.r.Read(z)
		for i, v := range z {
			if v > 96 && v < (96+26) {
				z[i] = byte((((int(v) - 97) + 13) % 26) + 97)
			}
		}

		if err == nil || err == io.EOF {
			if count > 0 {
				copy(b, z)
			}
		}
		return count, err
	}
}

type Interface interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type reverse struct {
	io.Reader
	Interface
}
/*
func (r reverse) Less(i, j int) bool {
	return true
}

func (r reverse) Len() int {
	return 0
}

func (r reverse) Swap(i, j int) {

}
*/

func Reverse(data Interface) io.Reader {
	return nil
}

type Circle struct {
	radius int
}

func (c *Circle) area() (area float64) {
	area = float64(math.Pi * float64(c.radius * c.radius))
	c.radius = 2
	return
}

func main() {
	c := &Circle{9}
	fmt.Println("The area of circle is ", c.area())
	fmt.Println("The radius of circle after computation is ", c.radius)
	var x interface{} = *new(reverse)
	val, ok := x.(io.Reader)
	if ok {
		v, ok := x.(reverse)
		if ok {
			fmt.Println("is reverse")
			Reverse(v)
		} else {
			fmt.Println("not reverse")
		}
		fmt.Println(val)
	} else {
		fmt.Printf("not ok %v %T", x, x)
	}
	s := strings.NewReader("lbh penpxrq gur pbqr!")
	r := rot13Reader{s}
	io.Copy(os.Stdout, &r)
}
