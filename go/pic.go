package main

import (
	"golang.org/x/tour/pic"
)

func Pic(dx, dy int) [][]uint8 {
	var ret [][]uint8 = make([][]uint8, dy, dy)
	for i := 0; i < dy; i++ {
		s := make([]uint8, dx, dx)
		for j := range s {
			if (j % 16) < 8 {
				s[j] = uint8((256 - (j * 16)) - i*4)
			} else {
				s[j] = uint8((j * 16) + (i * 4))
			}
		}
		ret[i] = s
	}

	return ret
}

func main() {
	pic.Show(Pic)
}
