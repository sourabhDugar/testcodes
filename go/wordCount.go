package main

import (
	"fmt"
	"strings"
	"golang.org/x/tour/wc"
)

func WordCount(s string) map[string]int {
	countMap := make(map[string]int)
	for _, val := range strings.Fields(s) {
		count, present := countMap[val]
		if present {
			countMap[val] = count + 1
		} else {
			countMap[val] = 1
		}
		fmt.Println(val)
	}
	return countMap
}

func main() {
	//WordCount(" foo bar baz ")
	wc.Test(WordCount)
}
