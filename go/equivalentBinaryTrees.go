package main

import (
	"fmt"
	"golang.org/x/tour/tree"
)

type elem struct {
	val int
	done bool
}
// Walk walks the tree t sending all values
// from the tree to the channel ch.
func Walk(t *tree.Tree, ch chan *elem, fn func(int) int) {
    	if t == nil {
		    return
    	}
	Walk(t.Left, ch, fn)
	ch <- &elem{fn(t.Value), false}
	Walk(t.Right, ch, fn)
}

func BeginWalk(root *tree.Tree, ch chan *elem, fn func(int) int) {
	Walk(root, ch, fn)
	ch <- &elem{fn(0), true}
}

// Same determines whether the trees
// t1 and t2 contain the same values.
func Same(t1, t2 *tree.Tree) bool {
	ch1 := make(chan *elem, 10)
	ch2 := make(chan *elem, 10)
	go BeginWalk(t1, ch1, printVal)
	go BeginWalk(t2, ch2, printVal)
	var done1, done2 bool = false, false

	for !done1 || !done2 {
		var i, j elem
		select {
		case i_ch1 := <-ch1:
			i = *i_ch1
			j = *<-ch2
			fmt.Printf("i:%d j:%d\n", i, j)
		case j_ch2 := <-ch2:
			j = *j_ch2
			i = *<-ch1
			fmt.Printf("i:%d j:%d\n", i, j)
		}
		if i != j {
			fmt.Println("unequal values", i, j)
			return false
		}
		if i.done {
			done1 = true
		}
		if j.done {
			done2 = true
		}
		if done1 && done2 {
			fmt.Println("All done .. going away .. bye bye!")
		}
	}
	fmt.Println("trees are equal")
	return true
}

func printVal(x int) int {
	//fmt.Println(x)
	return x
}

func main() {
	t1 := tree.New(3)
	t2 := tree.New(3)
	Same(t1, t2)
}
