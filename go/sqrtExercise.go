package main

import (
	"fmt"
	"math"
)

func deviation(root float64, numberToRoot float64) (deviation float64) {
	fromLib := math.Sqrt(numberToRoot)
	deviation = root - fromLib
	return
}

func subCalc(x float64, numberToRoot float64) (y float64) {
	piece := ((x * x) - numberToRoot) / float64(2*x)
	//fmt.Println("piece =", piece)
	y = x - piece
	return
}

func Sqrt(x float64) (float64, int, float64) {
	const (
		maxIter  = 11
		tolerate = 0.000002
	)
	for iter, guessed, root := 0, 0.0, float64(x/2); iter < maxIter; iter++ {
		root = subCalc(root, x)
		//fmt.Println("iterating...")
		if guessed < root {
			delta := root - guessed
			guessed = root + delta
		}

		if iter == (maxIter - 1) || (guessed - root) < tolerate {
			return root, iter, deviation(root, x)
		}
		guessed = root
	}
	return 0, 0, 0
}

func main() {
	fmt.Println(Sqrt(2))
	fmt.Println(Sqrt(3))
	fmt.Println(Sqrt(4))
	fmt.Println(Sqrt(5))
}
