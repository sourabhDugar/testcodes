package main

import (
	"fmt"
	"sync"
	"time"
)

type Fetcher interface {
	// Fetch returns the body of URL and
	// a slice of URLs found on that page.
	Fetch(url string) (body string, urls []string, err error)
}

// Crawl uses fetcher to recursively crawl
// pages starting with url, to a maximum of depth.
func Crawl(url string, depth int, fetcher Fetcher) {
	// TODO: Fetch URLs in parallel.
	// TODO: Don't fetch the same URL twice.
	// This implementation doesn't do either:
	if depth <= 0 {
		return
	}
	body, urls, err := fetcher.Fetch(url)
	if err != nil {
		fmt.Println(err)
		return
	}
	_ = syncPut(url, &fakeResult{body, urls}, true)
	fmt.Printf("found: %s %q \n", url, body)
	for _, u := range urls {
		if !syncPut(u, nil, false) {
			go Crawl(u, depth-1, fetcher)
		}
	}
	return
}

func syncPut(url string, res *fakeResult, force bool) bool {
	mux.Lock()
	defer mux.Unlock()

	_, present := cache[url]
	if !present {
		cache[url] = *new(fakeResult)
	}


	if force {
		cache[url] = *res
		urlsFetched <- url
	}


	return present
}

func main() {
	go Crawl("http://golang.org/", 4, fetcher)
	//count := 0
	boom := time.After(5000 * time.Millisecond)
	//tick := time.Tick(10 * time.Millisecond)
	do := true

	for do {
		select {
		case u := <-urlsFetched:
			fmt.Println("Fetched ", u)
		case <-boom:
			fmt.Println("timed out")
			do = false
		default:
			fmt.Println("     ..")
			time.Sleep(500 * time.Millisecond)
		}
	}
}

// fakeFetcher is Fetcher that returns canned results.
type fakeFetcher map[string]*fakeResult

type fakeResult struct {
	body string
	urls []string
}

func (f fakeFetcher) Fetch(url string) (string, []string, error) {
	if res, ok := f[url]; ok {
		return res.body, res.urls, nil
	}
	return "", nil, fmt.Errorf("not found: %s", url)
}

var cache map[string]fakeResult = make(map[string]fakeResult)
var mux sync.Mutex
var urlsFetched chan string = make(chan string, 1)

// fetcher is a populated fakeFetcher.
var fetcher = fakeFetcher{
	"http://golang.org/": &fakeResult{
		"The Go Programming Language",
		[]string{
			"http://golang.org/pkg/",
			"http://golang.org/cmd/",
		},
	},
	"http://golang.org/pkg/": &fakeResult{
		"Packages",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/",
			"http://golang.org/pkg/fmt/",
			"http://golang.org/pkg/os/",
		},
	},
	"http://golang.org/pkg/fmt/": &fakeResult{
		"Package fmt",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/pkg/os/": &fakeResult{
		"Package os",
		[]string{
			"http://golang.org/",
			"http://golang.org/pkg/",
		},
	},
	"http://golang.org/cmd/": &fakeResult{
		"Package cmd",
		[]string{
			"http://golang.org/",
			"http://golang.org/cmd/go",
		},
	},
}
