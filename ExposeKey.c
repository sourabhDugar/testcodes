#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

typedef struct data_and_keys {
    unsigned int   e1;
    unsigned int   e2;
#ifdef UPSET_INITIAL_LAYOUT
    unsigned char  cd1;
#endif
    unsigned short k1;
    unsigned char  k2;
    int            e3;
    int            e4;
    short          e5;
} data_and_keys_t;

#define offsetof_k1_in_d_n_k offsetof(struct data_and_keys, k1) * 8

#if (1)
    #define offsetof_k1_in_d_n_k_orig offsetof(struct data_and_keys, k1) * 8
    #define offsetof_k1_in_d_n_k_1 \
		((offsetof_k1_in_d_n_k_orig) - (sizeof(size_t) * 8))
struct test_struct_t {
    char l[offsetof_k1_in_d_n_k_1];
};
    #undef offsetof_k1_in_d_n_k
    #define offsetof_k1_in_d_n_k sizeof(size_t) * 8
#endif

typedef struct keyed_container {
    char    char_list[15];
    int     uk1;
    union {
        data_and_keys_t d_n_k;
        struct {
#ifdef offsetof_k1_in_d_n_k_1
            size_t : offsetof_k1_in_d_n_k;
            size_t : offsetof_k1_in_d_n_k_1;
#else
            size_t : offsetof_k1_in_d_n_k;
#endif
            unsigned short exposed_k1;
        };
    };
    int     uk2;

} keyed_container_t;


int
main (int argc, char *argv[])
{
    data_and_keys_t dnk1;
    dnk1.e1 = 55;
    dnk1.e2 = 95;
    dnk1.k1 = 875;
    dnk1.k2 = 'z';
    dnk1.e3 = 54859;
    dnk1.e4 = 5943;

    keyed_container_t kC1;
    memcpy(kC1.char_list, "disdavantage", sizeof("disadvantage"));
    kC1.uk1 = 959599;
    kC1.d_n_k = dnk1;
    printf("offset of k1 is : %lu\n", offsetof_k1_in_d_n_k);
#ifdef offsetof_k1_in_d_n_k_1
    printf("offset over sizeof(size_t) is %lu\n", offsetof_k1_in_d_n_k_1);
#endif
    printf ("exposed_k1 is : %u\n", kC1.exposed_k1);
#ifdef FORCE_STR_ERROR
    kC1.char_list[sizeof("disadvantage") - 1] = 's';
    kC1.char_list[sizeof("disadvantage")] = 's';
    kC1.char_list[sizeof("disadvantage") + 1] = 's';
#endif
    printf ("string member is : %s, len is %lu\n",
		   	kC1.char_list, strlen(kC1.char_list));
}

