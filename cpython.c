#include <Python/Python.h>
//from numpy.oldnumeric.precision import PyObject

static PyObject * 
john_system(PyObject *self, PyObject *args) {
    const char *cmd;
    int sts;
    /* parse args */
    if (!PyArg_ParseTuple(args, "s", &cmd))
        return (NULL);
    
    sts = system(cmd);
    
    return Py_BuildValue("i", sts);
}

PyMethodDef johnMethods[] = {
    {"johnsys",john_system,METH_VARARGS,"Execute a shell cmd"},
    {NULL, NULL, 0, NULL},
};


PyMODINIT_FUNC
initjohn (void)
{
    (void)Py_InitModule("john", johnMethods);
}

