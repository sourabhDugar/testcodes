#!/opt/local/bin/python2.7
from Queue import Queue, Empty
import signal
import time
import sys

def SigHndlr (signo, data):
	print "Recvd a signal"

def SignalSafe (delay):
	q = Queue()
	try:
		q.get(True, delay)
	except Empty: pass

while True:
	signal.signal(signal.SIGINT, SigHndlr)
	print "sleeping for 10 seconds"
	if sys.argv[1] == 'n':
		time.sleep(10)
	else:
		SignalSafe(10)

