#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

/*
 * Calls modified fopen using dlopen, dlsym
 * Tried LD_PRELOAD, somehow fails,
 * as MAC is unable to find the so while linking
 */
typedef FILE *(*fopen_td)(const char *filename, const char *perm);
int
main (int argc, char *argv[])
{

	FILE *testfile = 0;
	void *handle = dlopen("./fopenpre.so", RTLD_NOW);
	if (!handle)
		printf ("Failed to open the shared library fopenpre.so, %s\n",
			   	dlerror()), exit(1);
	fopen_td libFopen = NULL;
	libFopen = dlsym(handle, "fopen");
	const char *dlsym_error = 0;
	if ((dlsym_error = dlerror()) != NULL) {
		printf("Error finding the fopen symbol : %s\n", dlsym_error);
		exit(1);
	}
	testfile = libFopen(argv[1], "wa+");
	if (testfile)
		printf("openend file %s successfully\n", argv[1]);
	else
		exit(1);
	fclose(testfile);
}
