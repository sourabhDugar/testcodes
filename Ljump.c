#include <signal.h>
#include <setjmp.h>
#include <stdlib.h>
#include <stdio.h>

int i, j;
long T0;
jmp_buf Env;
void alarm_handler2 (int dummy);
void alarm_handler (int dummy)
{
	long t1;
	t1 = time(0) - T0;
	printf("%d seconds have passed j = %d. i = %d\n", t1, j, i);

	if (t1 >= 8) {
		printf("GAve up \n");
		/* When more than 8 secs pass
		 * we will jump back to main routine,
		 * to place where setjmp was called
		 * skipping re-arming of timer too :)
		 * And finally we will give up..
		 */
		longjmp(Env, 1);
	}
	/* arm alarm to deliver SIGALRM after 1 second */
	alarm(1);
	/* call the other signal handler*/
	signal(SIGALRM, alarm_handler2);

}

void alarm_handler2 (int dummy)
{
	long t1;
	t1 = time(0) - T0;
	printf("%d - 2 - seconds have passed j = %d. i = %d\n", t1, j, i);

	if (t1 >= 9) {
		printf("GAve up \n");
		/* When more than 9 secs pass
		 * we will jump back to main routine,
		 * to place where setjmp was called
		 * skipping re-arming of timer too :)
		 * And finally we will give up..
		 */
		longjmp(Env, 1);
	}
	/* arm alarm to deliver SIGALRM after 1 second */
	alarm(1);
	/* call the other signal handler*/
	signal(SIGALRM, alarm_handler);

}


int
main()
{
	/* install signal handler first */
	signal(SIGALRM, alarm_handler);
	/* set up the SIGALRM to be delivered in 1 second */
	alarm(1);

	if (setjmp(Env) != 0) {
		printf("Gave up : j = %d, i = %d\n", j, i);
		exit(1);
	}

	T0 = time(0);

	for (j = 0; j < 10000; j++) {
		for (i = 0; i< 1000000; i++);
	}
}
