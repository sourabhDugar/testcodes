#include <iostream>
#include <functional>
#include <list>
using namespace std;

template <typename X> 
struct identity {
    typedef X type;
};
int z;
typedef identity<int>::type r;
template <size_t T1, size_t T2, size_t T3, size_t T4>
struct Plus 
{
    //typedef typeof(typeof(T1) + typeof(T2)) type;
    enum { value = T1 + T2 + T3 + T4 };
};
template <typename T, T N>
struct integral_c {
    typedef T type;
    typedef T value_type;
    enum { value = N };
};

integral_c<short, 8> sh_c;
#if 0
template <>
struct Plus <8, 9, 1, 2>
{
    
};
#endif

template <typename T, T N>
class typeof {
};
template <typename T>
constexpr int typefunc (T *x) {
    return 0;
}

template <>
constexpr int typefunc<int> (int *x) {
    return 1;
}
template <>
constexpr int typefunc<short> (short *x) {
    return 2;
}

template <int v>
struct Int2Type
{
  enum { value = v };
};
template <>
constexpr int typefunc<Int2Type<0>> (Int2Type<0> *x) {
    return 3;
}

template <typename T, int isPolymorphic>
class NiftyContainer
{
  private:
    void DoSomething(T* pObj, Int2Type<1>)
    {
      //T* pCopy = pObj->Clone();
      cout << "1 called" << endl;
    }
    void DoSomething(T* pObj, Int2Type<0>)
    {
      T* pCopy = new T (*pObj);
      cout << "0 called" << endl;
    }
  public:
    void DoSomething(T* pObj)
    {
      DoSomething(pObj, Int2Type<isPolymorphic>());
    }
};
template <typename T, typename U>
class Conversion
{
    typedef char Small;                 // defines type Small
    class        Big { char dummy[3]; }; // defines type Big
    static Small Test (const U&);       // no implementation
    static Big   Test (...);            // no implementation
    static T     MakeT();               // no implementation
  public:
    enum { exists = sizeof(Test(MakeT())) == sizeof(Small) };
};
//typedef Plus<8, 9, 1, 2>::type d;
//typedef Plus<8, 9, 1, 2>::type d;
int df (int x) { return x; }
int (&s)(int) = df;
int
main () {
    NiftyContainer<int, 0> NiC;
    int *foo = new int;
    NiC.DoSomething(foo); 
    *foo = 10;
    /* a short type, declared by templating */
    typedef integral_c<short, 9> nine;
    
    long long  nine_2;
    nine::type nine_3;
    cout << typefunc(&nine_3);
    cout << "***" << endl;
    
    int zeb = Plus<integral_c<short, 10>::value, 9, 8, 9>::value;
    cout << "zeb is " << zeb << endl;
    cout << "***" << endl;
    //int *bar = std::ref(foo);
    int *bar(new int);
    (*bar) ++;
    cout << "bar " << *bar << " foo " << *foo << endl;
    delete foo;
    delete bar;
    cout << "bar " << *bar << endl;
    std::cout << "conversions ...." << std::endl;
    std::cout << Conversion < double , int >::exists << ' '
              << Conversion < char , char* >::exists << ' '
              << Conversion < size_t , std::list<int > >::exists << '\n';
    return 0;
}
