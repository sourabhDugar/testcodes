#include "treenode.h"
#include <string>
#include <memory>

using namespace sourabh;
using std::cout;
using std::endl;
using std::unique_ptr;
using std::move;
typedef unique_ptr<Node> unique_node;

int
main (int argc, char ** argv)
{
    unique_ptr<Node> node = (unique_ptr<Node>(new Node('R', nullptr, nullptr)));
    unique_ptr<Node> c1 = unique_ptr<Node>(new Node('c', new Node('z', nullptr, nullptr),
                                                          new Node('w',  nullptr, nullptr)));
    unique_ptr<Node> root = unique_ptr<Node>(new Node('0', nullptr, nullptr));
    node->left = std::move(c1);
    root->right = std::move(node);
   
    cout << root->public_key << endl;
    cout << root->right->public_key << endl;
    cout << root->right->left->public_key << endl; 
    cout << root->right->left->left->public_key << endl; 
    cout << root->right->left->right->public_key << endl;
    
    root->rotateRight(root->right.get()); 
    cout << "------" << endl;
    
    cout << root->public_key << endl;
    cout << root->right->public_key << endl;
    cout << root->right->left->public_key << endl; 
    cout << root->right->right->public_key << endl; 
    cout << root->right->right->left->public_key << endl; 

    root->rotateLeft(root->right.get());
    cout << "------" << endl;
    
    cout << root->public_key << endl;
    cout << root->right->public_key << endl;
    cout << root->right->left->public_key << endl; 
    cout << root->right->left->left->public_key << endl; 
    cout << root->right->left->right->public_key << endl;
    
#if 0
    //node->left = std::move(c1);
    //unique_ptr<Node> &left_ref = node->left->left;
    //cout << "public_key: " << (node)->left->public_key << endl;
    cout << c1.get() << endl;
    //swap(*(root->right), *c1);
    cout << "------" << endl;
    cout << c1.get() << endl;
    //node.swap(c1);
    //cout << node->public_key << endl;
    cout << "After swap" <<endl;
    //cout << left_ref->public_key << endl;
    //left_ref.reset();
#endif
    
}
