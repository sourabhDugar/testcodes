#include <memory>
#include <iostream>

#ifndef _TREENODE_H_
#define _TREENODE_H_

using std::unique_ptr;
using std::cout;
using std::endl;

namespace sourabh {
    class Node {
        private:
            char key;
        public:
            char public_key;
            std::unique_ptr<Node> right;
            std::unique_ptr<Node> left;
            Node (const char key, Node * left, Node * right) : key(key), right(right), left(left) {
                this->public_key = key;
            }
            void rotateRight(Node *n);
            void rotateLeft(Node *n);
            friend void swap(Node &, Node &);
            virtual ~Node() { std::cout << "Node destroyed: " << key << std::endl; }
    };
    

    void
    Node::rotateRight(Node *n)
    {
        unique_ptr<Node> tmp = std::move(n->left);
        swap(*tmp, *n);
        tmp->left = std::move(n->right);
        n->right = std::move(tmp);     
    }

    void
    Node::rotateLeft(Node *n)
    {
        unique_ptr<Node> tmp = std::move(n->right);
        swap(*tmp, *n);
        tmp->right = std::move(n->left);
        n->left = std::move(tmp);
    }

    void 
    swap(Node &x, Node &y)
    {
        using std::swap;
        (y.left).swap(x.left);
        (x.right).swap(y.right);
        swap(x.key, y.key);
        swap(x.public_key, y.public_key);
    }
}
#endif
