#include <iostream>
#include <algorithm>
#include <string>
#include <vector>

using namespace std;

class anagrams {
    private:
        const vector<string>   strs;
        int                    count_of_strs;
        vector<string>         dups;

    public:
        vector<int>            inds;
        anagrams(vector<string> &ins, int count) :  strs(ins), count_of_strs(count) {
            dups = ins;
            cout << "Anagrams initialized " << endl;
            for (vector<string>::iterator it = ins.begin(), end = ins.end();
                 it != end; ++it) {
                cout << *it << endl;
            }
            for (int i = 0; i < count_of_strs; ++i) {
                inds.push_back(i);
            }
            for (vector<string>::iterator it = dups.begin(), end = dups.end();
                 it != end; ++it) {
                sort((*it).begin(), (*it).end(), std::less<int>());
            }
        }

        void printByInds() {
            cout << "Printing by indices " << endl;
            for (vector<int>::iterator it = inds.begin(), end = inds.end();
                 it != end; ++it) {
                cout << strs[*it] << endl;    
            }
        }
        
        friend class anagram_str_compare;
        friend class anagram_sort_by_len;
};

class anagram_str_compare {
    private:
        const anagrams a;

    public:
         anagram_str_compare(const anagrams &a) : a(a) 
            { cerr << "Anagram compare by string value initialized" << endl; }
         bool operator()(int i, int j) {
            return static_cast<bool>(a.dups[i].compare(a.dups[j]));
         }
};

class anagram_sort_by_len {
    private:
        const anagrams a;

    public:
         anagram_sort_by_len(const anagrams &a) : a(a)
             { cerr << "Anagram compare by len initialized" << endl; }
         bool operator()(int i, int j) {
            return a.dups[i].length() > a.dups[j].length();
         }
};


int
main (int argc, char ** argv)
{
    vector<string> input {"abc", "bac", "aabc", "abac", "abc", "z"};
    anagrams ags(input, input.size());

    cout << endl;
    sort(ags.inds.begin(), ags.inds.end(), anagram_str_compare(ags));
    sort(ags.inds.begin(), ags.inds.end(), anagram_sort_by_len(ags));
    ags.printByInds();
}
