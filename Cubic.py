#!/opt/local/bin/python2.7
import numpy as np
import matplotlib.pyplot as plt

Wm = 600
beta = 0.2
C = 1
K =  ((Wm * beta) / C) ** (1.0/3)
print "K : %f Kcube : %f"%(K, K**3)

def Wcubic(t, Wm, beta, C, K):
	Wc = (C * ((t - K) ** 3.0)) + Wm
	return Wc

def WcArray(rng):
	WcA=[]
	for t in rng:
		WcA.append(Wcubic(t, Wm, beta, C, K))
	return WcA

xaxis = np.arange(0.0, 10.0, 0.2)
#plot
plt.plot(xaxis, WcArray(xaxis), 'm-d')
plt.show()
for t, Wc in zip(xaxis, WcArray(xaxis)):
	print t, Wc
