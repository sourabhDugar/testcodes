#!/opt/local/bin/python2.7
import sys
import os
import time
import logging
import watchdog.observers as wob
import watchdog.events as woe
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
import re
import pdb

swpMatch=re.compile('.*\.swp')
fs = wob.fsevents.FSEventsObserver()

class MyMatchedEventHandler(woe.RegexMatchingEventHandler):
	ignore_regexes=[swpMatch]
	def on_created(self, event):
		print "%s %s event : %s"%(["Dir" if event.is_directory else "File"][0], event.src_path, event.event_type)
		with open('acc_log.txt', 'a+') as log:
			log.write("%s %s event : %s"%(["Dir" if event.is_directory else "File"][0], event.src_path, event.event_type))

	def on_deleted(self, event):
		print "%s %s event : %s"%(["Dir" if event.is_directory else "File"][0], event.src_path, event.event_type)
	def on_modified(self, event):
		print "%s %s event : %s"%(["Dir" if event.is_directory else "File"][0], event.src_path, event.event_type)

if __name__ == "__main__":
	path = sys.argv[1] if len(sys.argv) > 1 else '.'
	mtchEvH = MyMatchedEventHandler()
	#pdb.set_trace()
	fs.setDaemon(True)
	fs.schedule(mtchEvH, path, recursive=True)
	fs.start()
	print "After Run()"
	fs.join()
'''
	fs.start()
	try:
		while True:
			time.sleep(1)
			if fs.isDaemon():
				sys.exit(0)
	except KeyboardInterrupt:
		fs.stop()
	fs.join()
'''
