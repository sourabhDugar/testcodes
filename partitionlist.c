#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct list_node list_node;
struct list_node {
    int val;
    list_node * next;
};

void
print_list_vals(list_node * head, list_node * end) {
    list_node * iter = head;
    for (;iter != end; iter = iter->next) {
        printf(" %d ->", iter->val);
    }
    printf("\n");
}

list_node *
part_list_around_num(list_node * head, int num)
{
    list_node * hilo_prev = NULL;
    list_node * lohi_prev = NULL;
    list_node * iter = head, *prev = head;
    
    for (; iter != NULL; iter = iter->next) {
        if (prev != iter && prev->val < num && iter->val >= num)
            if (hilo_prev != NULL && lohi_prev != NULL) {
                //remove list from hilo_prev->next to prev
                //and insert after lohi_prev->next
                print_list_vals(hilo_prev, iter);
                prev->next = lohi_prev->next;
                lohi_prev->next = hilo_prev->next;
                hilo_prev->next = iter;
                lohi_prev = prev;
                hilo_prev = NULL;
            }
        if (prev != iter && prev->val < num && iter->val >= num) {
            lohi_prev = prev;
        }
        if (prev != iter && prev->val >= num && iter->val < num) {
            hilo_prev = prev;
        }
        prev = iter;
    }
    if (hilo_prev != NULL) {
        prev->next = lohi_prev->next;
        lohi_prev->next = hilo_prev->next;
        hilo_prev->next = iter;
    }
    return head;
}
/**
 * links all the nodes greater than partition value 
 * to head1.next. At the end of iteration on original list
 * prev would point to the last node (as iter = prev and iter->next = prev->next = null)
 * We simply link the prev->next = head1.next
 */
list_node *
part_list2 (list_node * head, int num) {
    list_node head1;
    list_node * h1 = &head1;
    list_node * prev = head, * iter = head;

    for (;iter != NULL; prev = iter, iter = iter->next) {
        if (iter->val >= num) {
            h1->next = iter;
            prev->next = iter->next;
            iter->next = NULL;
            h1 = iter;
            iter = prev;
        }
    }
#ifdef DEBUG
    print_list_vals(head1.next, NULL);
    print_list_vals(head, NULL);
#endif
    prev->next = head1.next;
    return head;
}

list_node *
reverse_list (list_node * head) {
    list_node head1;
    list_node * h1 = &head1;
    list_node * iter = head;
    
    while (iter != NULL) {
        head = iter->next;
        iter->next = h1->next;
        h1->next = iter;
        iter = head;
    }
    return h1->next;
}

int
main (int argc, char ** argv)
{
    list_node * head = NULL;
#define SIZE 6
    //int vals[SIZE] = {3, 5, 8, 5, 10, 2, 1}; 
    int vals[SIZE] = {1, 4, 3, 2, 5, 2}; 
    int i = 0;
    list_node * prev = NULL;
    for (; i < SIZE; i++) {
        if (head == NULL) {
            head = malloc(sizeof(list_node));
            head->val = vals[i];
            prev = head;
        } else {
            list_node * new_node = malloc(sizeof(list_node));
            new_node->val = vals[i];
            prev->next = new_node;
            prev = new_node;
        }
    }
    prev->next = NULL;
    printf("Before : ");
    print_list_vals(head, NULL);
    part_list2(head, 3);
    //head = part_list_around_num(head, 3);
    printf("After : ");
    print_list_vals(head, NULL);
    head = reverse_list(head);
    print_list_vals(head, NULL);
}
