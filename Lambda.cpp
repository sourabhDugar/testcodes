#include <iostream>
#include <stdlib.h>
#include <vector>
#include <list>
#include <algorithm>
using namespace std;

#define list_init = { 1, 2, 3, 4, 5 };

int
main ()
{
    //vector<int> some_list(list_init, list_init + sizeof(list_init) /
    //                        sizeof(int));
	int         listinit[] = {1, 2, 3, 4, 5};
    vector<int> some_list (listinit, listinit+5);
    int         total = 0;
    int         value = 5;
	vector<int>::iterator it = some_list.begin();
	cout << "First element of the list is " << *it << endl;
    std:for_each(some_list.begin(), some_list.end(), [&total](int x) {
                        total += x;
                        });
    cout << "Total is " << total << endl;
}

