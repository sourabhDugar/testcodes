#include <iostream>

using namespace std;
typedef std::function<int(int)> show_type;

auto add_cont = [](const int& lhs, const int& rhs,
                   std::function<int(int)> k)  {
    return k(lhs + rhs);
};

auto mul_cont = [] (const int& lhs, const int& rhs,
                          std::function<int(int)> k) {
    
    return k(lhs * rhs);
};
auto show_cont = [](const int& v) { cout << v << endl; };
auto show_cont_int = [](const int& v) { cout << v << endl; return 1;};
auto sum_of_squares_cont = [&] (const int& lhs, const int& rhs,
                                show_type k) {
    
    return mul_cont(lhs, lhs, [&](const int& lhs2) {
                        return mul_cont(rhs, rhs, [&](const int& rhs2) {
                            return add_cont(lhs2, rhs2, k);
                            });
                    });
};

int
main (int argc, char** argv)
{
    int a = 5, b = 6;
    show_cont(a); show_cont(b);
    add_cont(5, 6, show_cont_int);
    /*
    mul_cont(a, a, [&](const int& ans1) {
                return mul_cont(b, b, [&](const int& ans2) {
                        show_cont_int(ans2 + ans1);
                        return 1;
                    });
            });
            */
    cout << sum_of_squares_cont(a, b, show_cont_int); 
}
