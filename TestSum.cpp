#include <iostream>

int f(int &a, int &b) {
  a = 3;
  b = 4;
  return a + b;
}
#if 0
int main() {
  int a = 1;
  int b = 2;
  int c = f(a, a);
  std::cout << a << b << c;
}

#include <iostream>

int main() {
  int a = 0;
  decltype((a)) b = b;
  b++;
  std::cout << a << b;
}
#endif

#if 0
class Ax {
public:
  Ax() { std::cout << 'a'; }
  ~Ax() { std::cout << 'A'; }
};

class Bx {
public:
  Bx() { std::cout << 'b'; }
  ~Bx() { std::cout << 'B'; }
  Ax ax;
};
struct A {
  A() { std::cout << "1"; }

  A(int) { std::cout << "2"; }

  A(std::initializer_list<int>) { std::cout << "3"; }
};

int main(int argc, char *argv[]) {
  A a1;
  A a2{};
  A a3{ 1 };
  A a4{ 1, 2 };
  Bx bx;
  1, bx;
}
#endif
#include <iostream>
#include <memory>
#include <vector>
#if 0

#define UDPTRANS_DEBUG(format, args...) \
    do {                                \
        printf(format, ##args);         \
    } while(0)

class C {
public:
  C() {}
  void foo()       { std::cout << "A"; }
  void foo() const { std::cout << "B"; }
};

struct S {
  std::vector<C> v;
  std::unique_ptr<C> u;
  C *const p;

  S()
    : v(1)
    , u(new C())
    , p(u.get())
  {}
};

int main() {
#if 0
   	S s;
  int *const a=(int *)malloc(sizeof(int));
  int const *b=(int *)malloc(sizeof (int));
  int c = 9;
  //a = &b;
  *a = 1;
  b = &c;
  const S &r = s;
  //r = &b;
  s.v[0].foo();
  s.u->foo();
  s.p->foo();

  r.v[0].foo();
  r.u->foo();
  r.p->foo();
#endif
  C c3;
  C *const c1= &c3;
  C c2;
  c1->foo();
  c2.foo();
  UDPTRANS_DEBUG("begin %d %d %d\n", c3, c2, c1);
}
#endif
#if 1
int x = 0;
#define X(a) if (a)  printf("Hi\n");
class A {
public:
  A() {
    std::cout << 'a';
    if (x++ == 3) {
      throw std::exception();
    }
  }
  ~A() { std::cout << 'A'; }
};
class D {
	public:
  D() { std::cout << 'd'; }
  ~D() { std::cout << 'D'; }

};

class B {
public:
  B() { std::cout << 'b'; }
  ~B() { std::cout << 'B'; }
  D d;
  A a;
};

void foo() { static B b; }

int main() {
  try {
    foo();
  } catch (std::exception &) {
    std::cout << 'c';
    foo();
  }
}
#endif
