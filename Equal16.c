#include <stdio.h>
#include <stdlib.h>

void
main (int argc, char *argv[])
{
	int sum = 0;

	sum = 95000;

	if (argc > 1) sum = atoi(argv[1]);

	if ((unsigned short )((sum >> 16) + (sum & 0xffff)) ==
		(unsigned short)(sum + (sum >> 16))) {
		printf ("Both methods yield equal results\n");
	} else {
		printf ("Both methods DO NOT yield equal results\n");
	}

}

