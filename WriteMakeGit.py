#!/opt/local/bin/python2.7
import os
import sys
import subprocess as sp
import re
cfile = re.compile("(.*)\.c")

def GetModFiles():
	p = sp.Popen('git diff --name-only', shell=True, stdout=sp.PIPE)
	files = p.communicate()[0]
	file_l = files.strip('\n').split('\n')
	return file_l

def Cfiles(file_list):
	for f in file_list:
		cf = cfile.match(f)
		if cf == None:
			continue
		if cf.group(1) != None:
			print cf.group(1)
			yield cf.group(1)
		else:
			print cf.groups()
			continue

def WriteAllTgt(file_list, make_file_name):
	with open(make_file_name, "wa+") as mk:
		allstr = "all :"
		for cf in Cfiles(file_list):
			allstr += cf + " "
		mk.writelines(allstr + '\n')

def AddExecs(file_list, make_file_name):
	with open(make_file_name, "a+") as mk:
		mk.seek(0, 2)
		for cf in Cfiles(file_list):
			mk.writelines(cf + ": " + cf + ".c\n")
			mk.writelines("\t$(CC) -g -o $@ $<\n")

WriteAllTgt(GetModFiles(), "custommk")
AddExecs(GetModFiles(), "custommk")
