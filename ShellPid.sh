#!/bin/bash
scriptname=$(basename $0)
lock="/var/run/${scriptname}"
exec 200>$lock
flock 200 || exit 1

pid=$$
echo $pid 1>&200

