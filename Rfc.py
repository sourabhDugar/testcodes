#!/opt/local/bin/python2.7
import mechanize
import pdb
import sys
import cookielib
import lxml.html as lxhtm

global match_found
match_found=0
#target_url='http://www.rfc-base.org/rfc-3261.html'
#target_url = 'http://wwwin.cisco.com'
#target_url = 'http://wwwin-ottawa.cisco.com/tfoggoa/Scrubber/showquery.html?query=renyang-76'
target_url='http://endets.cisco.com'

def ParseEndets(docfile):
    res = lxhtm.parse('endetsResp').getroot()
    #get tbody
    tbody = res.iter('tbody')
    totalBugs = 0
    print '%-15s'%'bug-id',
    print '%-120s'%'title'
    print '=' * 100
    for e in tbody:
        childrn = e.getchildren()
        for c in childrn:
            c2 = c.getchildren()
            #print c2
            for c3 in c2:
                c4 = c3.getchildren()
                #print c4
                for c5 in c4:
                    if 'title' in c5.keys():
                        break
                break
            else:
                 continue
            totalBugs = totalBugs + 1
            print '%-15s'% c5.values()[c5.keys().index('href')],
            print '%-120s'% c5.values()[c5.keys().index('title')]
        print 'TotalBugs : %-5d'% totalBugs

br = mechanize.Browser()
br.set_handle_equiv(True)
#br.set_handle_gzip(True)
br.set_handle_redirect(True)
br.set_handle_referer(True)
br.set_handle_robots(False)
cj = cookielib.LWPCookieJar()
br.set_cookiejar(cj)
password = ''
br.add_password(target_url, 'sdugar', password)

try:
    response=br.open(target_url)
except Exception, e:
    print e
    sys.exit(1)

import re
qm = re.compile('results/sdugar/AllBugs.*')
for form in br.forms():
    print "Form name:", form.name
    #print form
    #br.select_form(form.name)         # works when form has a name
    for control in form.controls:
       print control
       print control.type

for link in br.links():
    # Link(base_url='http://www.example.com/', url='http://www.rfc-editor.org/rfc/rfc2606.txt', text='RFC 2606', tag='a', attrs=[('href', 'http://www.rfc-editor.org/rfc/rfc2606.txt')])
    print(link.url)
    # http://www.rfc-editor.org/rfc/rfc2606.txt
    if qm.match(link.url):
        print('match found')
        global match_found
        match_found = 1
        # match found
        break
print '*_' * 50

ext_body_m = re.compile('.*<tbody>.*</tbody>', re.DOTALL)
if match_found:
    z=''
    with open('endetsResp','r+') as eResp:
        z=eResp.read()
    #commented out to avoid large o/p to screen
    #z_m=ext_body_m.match(z)
    z_m = None
    if z_m != None and len(z_m.groups()) > 1:
        print z_m.group(1)
    ParseEndets('endetsResp')
#pdb.set_trace()
