#include <sys/file.h>
#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

int
main (int argc, char *argv[])
{
	if (0) {
		perror("Invalid args");
	}
	int ret = 0;
	errno = 0;
	int lockfd = -1;
	char *endptr;
	//endptr[0] = '\0';
	//memset(endptr, 0, 120);
	lockfd = (int)strtoul(argv[1], (char **)&endptr, 10);
	if (endptr && argv[1][0] == endptr[0]) {
		fprintf(stdout, "Invalid argument - Not a digit\n");
		fprintf(stdout, "interpreting this as a file name : %s\n", argv[1]);
		if ((ret = access(endptr, F_OK))) {
			fprintf(stdout, "No file with the name\n");
			exit(1);
		} else {
			FILE *x = fopen(argv[1], "r");
			lockfd = fileno(x);
		}
	} else {
		/* fd supplied possibly */
	}
	fprintf(stdout, "fd : %d\n", (int)lockfd);
	if (flock(lockfd, LOCK_NB | LOCK_EX)) {
		fprintf(stdout, "Value of errno is : %d\n", errno);
		perror("FLOCK");
		return (1);
	} else {
		fprintf(stdout, "Lock acquired successfully\n");
	}
	return (0);
}

