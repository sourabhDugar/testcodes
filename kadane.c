#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int
main (int argc, char ** argv)
{
    int s = 0, e = 1, s_c = 0;
#define SIZE 7
    int in[SIZE] = { 10, 9, 6, 8, 9, 4, 15 };
    //int in[SIZE] = { 10, 8, 6, 4, 2, 0, -1 };
    int i = 1;
    int diffMax = in[1] - in[0];
    int curDiff = 0;
    for (; i < SIZE; i++) {
        curDiff += in[i] - in[i - 1];
        if (curDiff > diffMax) {
            diffMax = curDiff;
            s_c = s;
            e = i;
        } 
        if (curDiff < 0) {
            s = i;
            curDiff = 0;
        }
    }
    printf("Max rise is %d between %d and %d\n", in[e] - in [s_c], s_c, e);
}
