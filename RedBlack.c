#include "stdio.h"
#include "stdlib.h"
#include "errno.h"
#include <iostream>
//#define my_sizeof(type_) ((char *)(&type_ + 1) - (char *)(&type_))
#define my_sizeof(type_) (size_t)((((typeof(type_) *)0) + 1))
using namespace std;

struct test {
	int *a;
	struct test * t;
	int  b;
	int  c;
};
class A {
	int a;
	int b;
	int c;
	public:
		int f(int a) { cout << "func f" << endl; }
};

int
main (int argc, char *argv[])
{
	char *st[4];
	//errno = 10;
	//perror("Not Implemented");
	//exit(1);
	int x = 1;
	//int *y = &(x++);
	int z = (size_t)(((struct test *)0  + 1));//sizeof(struct test);
	int w =  sizeof(A);
	printf("z is %d w is %d\n", z, w);
	int i = 0;
	for (i=0; i<4; i++) {
		st[i] = (char *)malloc(10);
		st[i][9] = '\0';
		st[i][8] = 'E';
		st[i][7] = 'z';
		st[i][6] = 'm';
		st[i][5] = 'n';
	}
	for (i=0; i<4; i++)
		strncpy(st[i], "validateradsf", 9);
	for (i=0; i<4; i++)
		printf("string %d is %s\n", i, st[i]);
}
