#include <stdio.h>
#include <stdlib.h>
#include <search.h>

/**
 * Celebrity is someone who knows nobody.
 * But everybody knows him.
 * If we construct a graph, it basically means any random walk of the graph without visiting same node twice, should ultimately 
 * end at the celebrity.
 * The idea above is implemented in the code below.
 */
#define MAX 5
char KNOWS[MAX][MAX] = {{1,0,0,1,0}, 
                        {1,1,0,1,1}, 
                        {0,1,0,1,0}, 
                        {0,0,0,1,0},
                        {0,0,0,1,1}};

char
knows(int i, int j)
{
    // you always know self :-), but lets return false for the sake of this problem.
    if (i == j) return 0;
    return KNOWS[i][j]; 
}

int
main (int argc, char ** argv)
{
    int iter = 0;
    int inner_iter = 0;
    int cel_index = -1;
    (void)hcreate(MAX);
    ENTRY item;
    
    while (iter < MAX) {
        for (; inner_iter < MAX; ++inner_iter) {
            if (knows(iter, inner_iter)) {
                cel_index = -1;
                //adding entry to the hash table is to mark it visited
                item.key = malloc(sizeof(char) * 2);
                sprintf(item.key, "%d", inner_iter);
                if (hsearch(item, FIND) == NULL) {
                    (void)hsearch(item, ENTER);
                    iter = cel_index = inner_iter;
                    inner_iter = 0;
                    break;
                }
                //end of mark visited code
            }
        }      
        printf("The celebrity is at index %d\n", cel_index);
        iter = MAX;
    }
    (void)hdestroy(); 
    return 0;
}
