//
// Created by Sourabh Dugar on 2/7/16.
//
#include <list>
#include <memory>
#ifndef TESTCODES_GRAPHHEADER_H
#define TESTCODES_GRAPHHEADER_H
#define MY_NAMESPACE_BEGIN(X) namespace X __attribute__ ((__visibility__("default"))) {

#define MY_NAMESPACE_END }

MY_NAMESPACE_BEGIN(sourabh)

using namespace std;

class IterBegin {
public:
    virtual void operator()(int i) {}
};

class IterEnd {
public:
    virtual void operator()(int i, int j) {}
};

class Iter {
public:
    virtual void operator()(int i, int j) {}
};

class Graph
{
    int V;    // No. of vertices
    list<int> *adj;    // adjacency std::lists
public:
    Graph(int V);  // Constructor
    void addEdge(int v, int w); // to add an edge to graph
    void DFS();
    void DFS(unique_ptr<IterBegin, int (*)(IterBegin *)> b, Iter *iter);  // prints all vertices in DFS manner
    // prints all not yet visited vertices reachable from s
    //void DFSUtil(int s, bool visited[]);
    void DFSUtil(int s, bool visited[], Iter *iter);
};
MY_NAMESPACE_END
#endif //TESTCODES_GRAPHHEADER_H
