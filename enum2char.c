#include <stdio.h>
#include <stdlib.h>

/*__BEGIN_GLOBALS__*/
typedef enum {
    one=0,
    two=2,
    three=3
} my_enum;
const char *low_to_high = "from low to high";
const char *high_to_low = "from high to low";
/*__END_GLOBALS__*/

typedef enum stages_ {
    IF = 0,
    ID,
    IS,
    EX,
    WB,
    MAX,
} stages_t;
typedef void (*funPtr)(int);
void myfunc (int a)
{
	a++;
}

funPtr funPtrs[] = {
	[0] = NULL,
	[1] = myfunc,
};
typedef struct intPtr {
    int *intP;
} intPtr;

typedef struct INT {
    intPtr integer;
} INT;

typedef struct TWOINT {
    INT *one;
    INT *two;
} TWOINT;

TWOINT my_int;

char *strings[] = {
    "one",
    "two",
    "three",
};


unsigned int duration[MAX - 1];
stages_t     curr_stage=MAX;

const char * thisIsHowMyStackGrows (char *caller);

char *
tellMyEndianness (void *dummy,
                  void *dummy2)
{
    int e = *(int *)dummy;
    thisIsHowMyStackGrows((void *)&e);
    if (*(char *)&e)
        return ("little endian");
    else
        return ("big endian");
}

const char *
thisIsHowMyStackGrows (char *caller)
{
    char callee;

    if (&callee < caller)
        return (high_to_low);
    else
        return (low_to_high);
}

const char *
thisIsHowMyHeapGrows (char *caller)
{
    char *callee = malloc(sizeof(char));
    thisIsHowMyStackGrows((void *)callee);
    if (callee < caller)
        return (high_to_low);
    else
        return (low_to_high);
}

typeof (char *[4]) y;
typeof (typeof(char *)[4]) y2;
static int TestFunc
();

static int TestFunc (int a) {
    return (a * 10);
}

int
main (int argc, char *argv[])
{
	int ade;
    funPtrs[1](ade);
	long *self_ptr = (long *)&self_ptr;
    my_enum decision = 2;
    int dummy_pass = 1;
    char decision_val = decision;
    char *caller_malloc = malloc(sizeof(char));
    my_int.one = malloc(sizeof(INT));
    printf("MY TWO INT -- %p", &my_int.one->integer);
    if (decision_val == 2)
        printf("expected dec\n");
    else
        printf("not expected\n");
    printf("%s.\n", tellMyEndianness(&dummy_pass, &dummy_pass));
    printf("I am %s.\n", (*(char *)&decision) ? "little endian" : "big endian");
    printf("My stack grows %s.\n", thisIsHowMyStackGrows((char *)&decision)
                               /*(&a[0] < &a[3]) ? low_to_high : high_to_low*/);
    printf("My heap grows %s.\n", thisIsHowMyHeapGrows(caller_malloc));
    int *aCastToVoid=&dummy_pass;
    *(void **)aCastToVoid=NULL;
    return (1);
}
