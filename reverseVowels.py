#!/usr/local/bin/python2.7
import sys
import pdb
def reverse_vowels(str_):
    """
    Given a string, returns a string with the order of the vowels reversed but all other chars
    unchanged
    """
    tmp = list(str_)
    positions_and_vowels = [(index, char) for index, char in enumerate(tmp) if char in 'aeiou']
    positions, vowels = [x[0] for x in positions_and_vowels], [x[1] for x in positions_and_vowels]
    vowels.reverse()
    for index, char in zip(positions, vowels):
        tmp[index] = char
    return tmp

if len(sys.argv) < 2  and __name__ != 'main':
	print("Please provide string to reverse vowels in as an arg")
	sys.exit(1)
print('name of the prog is : %s', sys.argv[0])
print 'reverseVowels.py' in sys.argv[0]
print len(sys.argv)
print '=============='
#if (__name__ == 'main') and ('reverseVowels.py' in sys.argv[0]):
if __name__ == '__main__':
	print ''.join(reverse_vowels(sys.argv[1]))


