#!/opt/local/bin/python2.7
import os
import time
import sys

class square(object):
	def __init__(self, i, j):
		self.i = i
		self.j = j
	def ver(self, v):
		self.v = v
	@property
	def VER(self):
		return self.v

class grid(object):
	sq = {}
	def __init__(self, d):
		for i in range(0, d):
			for j in range(0, d):
				self.sq[(i, j)] = square(i, j)

class Stack(object):
	def __init__(self, d):
		self.arr = []
	def push(self, e):
		self.arr.append(e)
	def pop(self):
		return self.arr.pop()

def sqNbr(G, sqx, sqy, ml, mb):
	#z = [(x, y) for x in xrange(sqx - 1, sqx + 2) for y in xrange(sqy - 1, sqy + 2) if (x, y) != (sqx, sqy)]
	#yield [(x, y) for (x, y) in z]
	for x in xrange(sqx - 1, sqx + 2):
		for y in xrange(sqy - 1, sqy + 2):
			if (x, y) != (sqx, sqy) and x < mb and x >= 0 and y >= 0 and y < ml:
				G.sq[(x, y)].ver(2)
				yield G.sq[(x, y)]

def ApplyConwayRules(sqx, sqy, ml, mb):
	NumAlive = 0
	NumDead = 0
	for (i, j) in sqNbr(sqx, sqy, ml, mb):
		if state[(i, j)]:
			NumAlive = NumAlive + 1
		else:
			NumDead = NumDead + 1


G = grid(5)
for sq in sqNbr(G, 0, 1, 5, 5):
		print sq.i, sq.j, sq.VER

print '-' * 25 * 10
