
#include <iostream>
#include "stdio.h"
#include <string>
using namespace std;
//doesn't work...
namespace my_ptr {
	template <typename T>
	T operator* (T *t) {
		cout << "my deref called" << endl;
		return *t;
	}
}
//doesn't work...

class T {
    public:
	int t_var;
    virtual void ft(void) = 0;
	virtual void operator() (unsigned) const = 0;
	void foo2 (void)
	{
		cout << "T::foo2 called "<<endl;
	}
};

class I : public T {
    T &t;

    public:
    void
    foo (void)
    {
        cout << "I::foo called" << endl;
        t(1);
    }
	virtual void ft(void) { }
	virtual void operator() (unsigned x) const { }
    I(T &x) : t(x) { cout << "I:I(T *x) called" << endl;}
    template <typename Z>
    I(Z &z) : t(z) {cout << "I:I<>() called" << endl;}
    ~I()  { cout << "I:~I() called" << endl; }
};

class S : public T {
	virtual void ft(void) { }
	virtual void operator() (unsigned x) const { }
	public:
	int t_var;
};

class A :  public S {
    public:
    I i;
    A()  : i(*this)  { cout << "A::A() called" << endl; f(); }
    ~A() { cout << "A::~A() called" <<endl; f(); }

    virtual void
    f (void)
    {
        cout << "A::f() called" << endl;
        i.foo();
    }

	void foo2 (void)
	{
		S::t_var = 10;
		T::foo2();
		cout << T::t_var << endl;
	}

    virtual void
	operator() (unsigned x) const
    {
        cout << x <<":A::() called" << endl;
    }

    virtual void
    ft (void)
    {
        cout << "A::ft() called" << endl;
    }

};

class B  : public S {
    public:
    I i;
    B()   : i(*this)  {  cout << "B::B() called" << endl ; f(); }
    ~B() { cout << "B::~B() called" << endl; f(); }
    B(const int *a) : i(*this)  {}
    B(const char *a) : i(*this) {}
    virtual void
    f (void)
    {
        cout << "B::f() called" << endl;
        i.foo();
    }
	void foo2 (void)
	{
		S::foo2();
	}
    virtual void
    operator() (unsigned x) const
    {
        cout << x <<":B::() called" << endl;
    }

    virtual void
    ft (void)
    {
        cout << "B::ft() called" << endl;
    }

	template <typename T>
	string TYPE(T t) { return "unknown"; }
	//template overloading...
	template <typename T>
	string TYPE(T *t) { return "bad unknown"; }
    //Cast template from source type self to destination type T
	template <typename T>
	operator T*()
	{
        //Dummy to cast to T* and get a type
		B z;
		// overlaod resolution will select string TYPE(T *t) template
		// if we do not again explictly cast &z -> (T *) we will get an error
		// as compiler doesn't know how to make conversion from int * -> char * in our case..
		// However if &z is replaced with z casting is not necessary, since int can implicitly
		// convert to char resulting in a match with string TYPE (T t) always...
		// The commented out casting will again shift preference to 1st template
        // If we were to use (T *)z as the typecasted arg below we will run into an infinite
        // recursion as everytime this function will be called to convert from B -> T* until
        // we run out of stack!!
		cout << "Conversion function allocating new " << TYPE(/*(T)(long)*/(T *)&z) << endl;
		T *x = new T;
		return (x);
	}

};

//Template specialization
//Error occurs when I specialize this in class scope..
template<>
string B::TYPE<int>(int *x) {
	return ("int");
}
template<>
string B::TYPE<char>(char *x) {
	return ("char");
}
template <typename T>
class C ;

template<>
class C<char> : public S {
  public:
    I i;
    C()   : i(*this)  {  cout << "C::C() called" << endl ; f(); }
    ~C() { cout << "C::~C() called" << endl; f(); }

    virtual void
    f (void)
    {
        cout << "C::f() called" << endl;
        i.foo();
    }
	void foo2 (void)
	{
		S::foo2();
	}
    virtual void
    operator() (unsigned x) const
    {
        cout << x <<":C::() called" << endl;
    }

    virtual void
    ft (void)
    {
        cout << "C::ft() called" << endl;
    }

	template <typename T>
	string TYPE();
	//template<>
	//string TYPE<char>() {
	//	return ("char");
	//}

	template <typename T>
	operator T*()
	{
		cout << "Conversion function allocating new " << TYPE<T>() << endl;
		T *x = new T;
		return (x);
	}
};


#if 1
template <typename Te>
class Am
{
    public:
        template <typename T>
        Am(T t) : t_(t) { //cout << "Am _t is " << t_ << endl;
               }

    private:
        Te t_;
};

class SomeType  {
    int number;

public:
    SomeType(int new_number=42) :
	   	number(new_number)
   	{ cout << "value of new_number is 42" << endl; }
   // SomeType() : SomeType(42) {}
};

#endif
int
main (int argc, char *argv[])
{
    B b_;
	A a_;
    int z_z = 5;
    //B m_ = &z_z;//works with appropriate copy constructor;;
	C<char> c_;
	a_.foo2(); b_.foo2();
	B *b2 = new B();
	 {
	using namespace my_ptr;
	*b2;
	 }
	 int *x_num = (int *)b_;
	 char *x_str = (char *)b_;
	 delete x_num;
	 delete x_str;
	//I i11(*b_);
	//S::foo2();
    //Am<B> a(b);
    T *te; //<--- foo() is uncallable in this object
		   // as it is not defined anywhere...
    cout << "-----------" << endl;
    I n(*te);
	try  {
		//n.foo();
	} catch(std::exception& e) {
		cout << e.what() << endl;
	}
	SomeType S = SomeType();
	cout << "rvalue , lvalue ref" << endl;

	int a = 10;
	int& al = a;
	string orig("hello");
	cout << "string before move " << orig << endl;
	//string&& ar = "Ringer";
	//ar = std::move(orig);
	//cout << "string after move " << orig << endl;
	cout << "string rval ref after move " << orig << endl;
	char *z;
	int  *b;

	//z = (char *)&b;
	//*z = (char )((long)b);//illegal, just to check if int * can be assigned (or implicitly converted) to char
	/*
	al = 19;
	cout << "a is " << a << endl;
	ar = 85;
	cout << "a is " << a << endl;
	a = 95;
	cout << "a is " << a << endl;
	*/
}



