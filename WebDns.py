#!/opt/local/bin/python2.7
import time
import requests as rq
import pdb
import re
import sys

if len(sys.argv) > 1:
	int_to_convert = sys.argv[1]
else:
	int_to_convert = raw_input('int_to_convert_to_ip>')
res_match = re.compile('The IP Number 335740928 converts to the IP Address\s(\d+\.\d+\.\d+\.\d+)')
url = 'http://www.webdnstools.com/dnstools/ipaddress'
#form = {'input': '335740928', 'button': 'convert', 'actiontype': 'toip', 'ipver': '4'}
payload = 'input='+ str(int_to_convert) + '&actiontype=toip&ipver=4&button=Convert'
r = rq.post(url, data=payload)
result = res_match.search(r.text)
#pdb.set_trace()
if result != None and len(result.groups()) > 0:
	print "Ip equivalent is: " + result.group(1)
