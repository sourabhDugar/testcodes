#!/bin/bash

set -e

scriptname=$(basename $0)
echo $scriptname
echo -------------
lock="${scriptname}_lock"
echo $lock
echo -------------

exec 210> $lock
[ 1 -eq 0 ] || ./flock 210 || (echo "No lock acquired"; exit 1) || [ $? -ne 1 ] || exit 1

## The code:
echo here
pid=$$
echo $pid 1>&210
sleep 60
echo "Hello world"
