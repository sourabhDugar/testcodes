#include <stdio.h>
#include <stdlib.h>
#include <sys/resource.h>
#include <signal.h>

char *null;
//__attribute__ works on mac
const int premain_wrapper(int x) __attribute__ ((constructor));
//This is supposed to work apparently, doesn't work on Mac...
#pragma init (premain_wrapper)
const int
premain_wrapper(int x)
{
	printf("This is before main\n");
	return (1);
}
static const int (*register_dummy)(int) = (premain_wrapper);

void
sigsegv_handler(int signo)
{
	fprintf(stdout,"Test failed\n");
	exit(22);
}

/*
 *Invalid only in cpp initializer not being
 * a compile time constant is allowed
 */
//int my_constant = premain_wrapper(10);
/*
 * 0th bit will be 0 here, and shift to obtain this will be 31
 * This is becoz 0 is the MSB and to obtain msb a 31 bit shift is needed..
 * However, input passed is 0, as msb is the 0th bit when viewing from left
 * to right...
 * we do a 31 - bit % 32 using ~bit & 1f to do the right conversion...
 * ~bit is same as 31 - bit (if bit is less than 31), but since it is more
 * than 6 bits, bits greater than 6 will be 1 due to ~, and hence we do an
 * AND with 0x1f(31) to filter out these bits...
 */
int
main (int argc, char *argv[])
{
	struct rlimit rl = {0, 0};
	int rc;
	(void)register_dummy;
	//struct sigaction sa;
	//sa.sa_handler = sigsegv_handler;
	//sigaction(SIGSEGV, &sa, NULL);
	signal(SIGSEGV, sigsegv_handler);
	rc = getrlimit(RLIMIT_STACK, &rl);
	//fprintf(stdout, "soft lmt : %llu, cieling : %llu\n", rl.rlim_cur, rl.rlim_max);
	rl.rlim_cur = 4000;
	rl.rlim_max = 6000;
	rc = 1;
	//rc = setrlimit(RLIMIT_STACK, &rl);
	if (!rc) {
		rc = getrlimit(RLIMIT_STACK, &rl);
		//fprintf(stdout, "soft lmt : %llu, cieling : %llu\n", rl.rlim_cur, rl.rlim_max);
		//char *null = 0;
		*null = 1;
		free(null);
		null = 0;
	} //else
		//perror("setrlimit");
	char a[4]={0x9, 0x6, 0x1, 0x0};
	//                          ^The first bit from left
	//                          is the MSB
	//                          (This becomes MSB when we cast this char
	//                          array to an int)
	//                          And the below logic will
	//                          give this as the first bit
	//                          from left too
	//                          However we really need the
	//                          0 from 0x9 to be
	//                          the first bit
	//                          This would happen with
	//                          bigendian systems as 0x9's 0 will
	//                          be MSB
	//                          One way of achieving the right result is
	//                          doing an ntohl on the shift amount
	//                          for e.g. if you need the 0th bit, we will
	//                          do a 31 bit shift, giving us the left most
	//                          zero of 0x0. Now do an ntohl on this
	//                          we would have a pattern like 0x0 0x0 0x0 0x80
	//                          giving us the left most zero of 0x09
	//                          similarly for obtaining the LSB 1 of 0x09
	//                          we need to shift by 24 on BE systems
	//                          on BE 0x01 0x0 0x0 0x0
	//                          same on LE 0x0 0x0 0x0 0x01
	//                          XOR with   0x9 0x6 0x0 0x00 gives right most (LSB) 0
	//                          of 0x0
	//                         ntohl shift 0x1 0x0 0x0 0x0
	//                          now we obtain LSB or right most 1 of 0x9
	//                          NOTE NOTE NOTE : The program currently does not
	//                          implement any of the above, hence you get 1 of 0x9
	//                          as the 31st bit (when it should be really the 0th bit)
	if (argc <= 1) {
		exit(1);
	}
	int pos = atoi(argv[1]);
	int bit = pos;
	int *findin = (int *)a;
	//*((volatile int *)0);
	bit = ~bit & 0x1f; //same as (31 - bit) % 32
	printf("bit%ds:%d:%d\n",pos,bit, !!(*findin & (1 << bit)));

}
