#!/opt/local/bin/python2.7
import pexpect
import os
import sys
import pdb

#sys.argv[0] is prog name...
password = ''
def SShToServer(args):
	#print dir(pexpect)
	if len(sys.argv) < 2:
		print "Please specify IP or Hostname to connect to"
		sys.exit(1)
	else:
		ipOrHostname = sys.argv[1]
	p = pexpect.spawn("ssh " + ipOrHostname)
	while True:
		#print '*' * 50
		#Doesn't seem workable with just expecting '\r'
		#Seems that I have to expect assword: directly..
		#otherwise doesn't work at all...
		p.expect(['\r\n', '\n', '\r', 'assword:', 'yes/no'])
		# following doesn't seem to work in error cases
		# i.e. complete lines are not printed, some o/p
		# remains in the buffer and Ctrl-C causes it to
		# be printed, or adding a '\r' after the ,
		# That makes rest of the o/p look bad,,,
		print p.before + p.after,
		if 'yes' in p.after:
			p.send('yes\r')
		if 'assword:' in p.after:
			break
	#Can be used to echo the password on to the screen
	#p.setecho(True)
	if password == '':
		print "no password specified, aborting"
		return None
	p.send(password)
	p.expect('$')
	p.send('\r')
	p.expect('$')
	#p.write_to_stdout(p.before)
	#print '\r'
	#p.interact()
	#return rettxt
	#p.close()
	return p

txt = ''
p1 = None
p1 = SShToServer(sys.argv)
p2 = SShToServer([sys.argv[0], sys.argv[2]])

#Reset the txt buffer...
txt = ''
if p1 != None:
	p1.send('ls -lhrt \r')
	while True:
		try:
			p1.expect('\r', timeout=5)
		except Exception, e:
			break
		txt = txt + p1.before
	print '<p1>'
	print txt + '\r'
	print '</p1>'
	txt = ''
	p1.send('\r')
	p1.send('cd testpython\r')
	p1.expect('$')
	p1.send('ls\r')
	while True:
		try:
			p1.expect(['\r'], timeout=1)
		except Exception, e:
			break
		#print p1.before, "before"
		txt = txt + p1.before
	print '*' * 10
	print p1.after
	print '*' * 10
	print '<p1-2>'
	print txt
	print '</p1-2>'
	p1.send('\n')
	p1.expect('$', timeout=1)
	print '*' * 10
	print 'Z' * 15 , p1.before, 'X' * 15, p1.after, 'Y' * 15
	print '*' * 10
	p1.send('ls\r')
	p1.expect('$')
	print '*' * 10
	print 'Z' * 15 , p1.before, 'X' * 15, p1.after, 'Y' * 15
	print '*' * 10


p2 = None
#Reset the txt buffer here...
txt = ''
if p2 != None:
	p2.send('ls\r')
	while True:
		try:
			p2.expect('\r', timeout=1)
		except Exception, e:
			break
		txt = txt + p2.before
	print '<p2>'
	print txt
	print '</p2>'
