#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

pthread_mutex_t  odd_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t   odd_cond = PTHREAD_COND_INITIALIZER;
int              ODD = 0;
int              count = 0;
int              MAX = 0;


void
*print (void *odd_in)
{
    int odd = (int *)(odd_in);

    while (count <= MAX) {
        if (odd != 1) {
            int local_count = 0;
            pthread_mutex_lock(&odd_mutex);
            printf( "OR\n");

            //wait for the even thread to print
            while (ODD == 0) {
                printf( "OW\n");
				if (count >= MAX) {
					pthread_mutex_unlock(&odd_mutex);
					pthread_exit(NULL);
				}
                //I was wrongly woken, relaying signal
                pthread_cond_signal(&odd_cond);
                pthread_cond_wait(&odd_cond, &odd_mutex);
            }
            count++;
            local_count = count;
            ODD = 0;
                printf( "OP\n");

            if (local_count <= MAX) {
                fprintf(stdout, "%d \n", local_count);
                //fprintf(stdout, "'%d' ", odd);
            }
            //signal an even thread if waiting
            printf( "OS\n");
            pthread_cond_signal(&odd_cond);
            //unlock the mutex for the next thread to acquire
            pthread_mutex_unlock(&odd_mutex);

        } else if (odd == 1){
            int local_count = 0;
            pthread_mutex_lock(&odd_mutex);
            printf( "ER\n");

            //wait for an odd thread to print
            while (ODD == 1) {
                printf( "EW\n");
				if (count >= MAX) {
					pthread_mutex_unlock(&odd_mutex);
					pthread_exit(NULL);
				}
                //I was wrongly woken, relaying signal
                pthread_cond_signal(&odd_cond);
                pthread_cond_wait(&odd_cond, &odd_mutex);
            }
            count++;
            local_count = count;
            ODD = 1;
                printf( "EP\n");

            if (local_count <= MAX) {
                fprintf(stdout, "%d \n", local_count);
                //fprintf(stdout, "'%d' ", odd);
            }
            //signal if there is an odd printing thread waiting
            printf( "ES\n");
            pthread_cond_signal(&odd_cond);
            //unlock the mutex for the odd thread to print
            pthread_mutex_unlock(&odd_mutex);

        } else {
        }
    }
    pthread_exit(NULL);
}

void
main (int argc, char **argv)
{
    pthread_t tid[6];
    int       i = 0;

    if (argc < 2) {
        printf( "usage: %s MAX-Count\n", argv[0]);
        exit(1);
    }

    MAX = atoi(argv[1]);

    pthread_create(&tid[1], NULL, print, (void *)0);
    pthread_create(&tid[0], NULL, print, (void *)1);
    pthread_create(&tid[3], NULL, print, (void *)0);
    pthread_create(&tid[2], NULL, print, (void *)1);
    pthread_create(&tid[5], NULL, print, (void *)0);
    pthread_create(&tid[4], NULL, print, (void *)1);
    pthread_exit(NULL);
    exit(0);
}

