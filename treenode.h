#include <iostream>
#include <queue>
#define MARKER ')'
#define N 5
using namespace std;
#ifndef __TREENODE_H_
#define __TREENODE_H_
namespace sourabh { 
// A node of N-ary tree
class Node {
private:
   int nodeCount;
public:
   Node(char key) : nodeCount(0), key(key) { }
   const char key;
   const Node *child[N];  // An array of pointers for N children
   
   static Node *newNode(const char);
   int addChild(const Node *);
   inline int getChildCount() const { return this->nodeCount; }

   static const Node * marker; 
};

const Node * Node::marker = new Node(MARKER);
 
// A utility function to create a new N-ary tree node
Node *
Node::newNode(const char key)
{
    Node *temp = new Node(key);
    for (int i = 0; i < N; i++)
        temp->child[i] = NULL;
    return temp;
}

int
Node::addChild(Node const * child)
{
    this->child[nodeCount++] = child;
    return nodeCount;
}
}
#endif
