#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void
swap (int *arr, int i, int j) {
    arr[i] ^= arr[j];
    arr[j] ^= arr[i];
    arr[i] ^= arr[j];
}

void
set_num (int *arr, int num, int * len)
{
    int i = 0;
    while (i < *len) {
        if (arr[i] == num) {
            swap(arr, i, ((*len)--) - 1);
        } else {
            i++;
        }
    }   
}

void
sort0123 (int *arr, int * len)
{
    int lo = 0;
    int mid = 1;
    int hi = (*len) - 1;
    int hi3 = hi;
    
    while (mid < hi) {
        switch (arr[mid]) {
            case 0:
                swap(arr, lo++, mid++);
                break;
            case 1:
                mid++;
                break;
            case 2:
                swap(arr, hi--, mid);
                break;
            case 3:
                swap(arr, hi--, mid);
                if (hi3 > hi + 1) {
                    swap(arr, hi3--, hi + 1);
                }
                break;
        }
    }
}

int
main (int argc, char ** argv)
{
    int test_arr[16] = {1, 1, 0, 1, 2, 0, 0,3, 3, 1, 1, 0, 2, 1, 0, 3};
    int len = 16, len_cp = 16;
    /*
    set_num(test_arr, 2, &len);
    set_num(test_arr, 1, &len);
    set_num(test_arr, 0, &len);
    */
    sort0123(test_arr, &len);
    int iter = 0;
    for (; iter < len_cp; iter++) {
        printf("%d, ", test_arr[iter]);
    }
    printf("\n");
}
