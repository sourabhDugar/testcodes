#include <iostream>
using namespace std;


template <typename Sig>
class func {
    public:
    typedef Sig sign;
};
template <>
class func<int (int)> {
    public:
    typedef int sign(int) ;
    int (*orig_f) (int);
    int *s(int);
    //func (int (*in_f) (int)) : orig_f(in_f) {  } 
    func () :orig_f(0) {cout << "default const called" << endl;}
    int operator() (int x) { return orig_f ? orig_f(x) : 5; }
    int operator() (void) {cout << "() () called" << endl; return 0;}
    void operator= (int (*x)(int)) { cout << "= operator called" << endl; this->orig_f = x; }
};

class dfunc {
    public:
    int operator()(int x) { return 0; }
};

dfunc dfuncInst = dfunc();

int d (int x) { cout <<"d called "<< x << endl; return 0; }

//func<int (int)> f(d);
int x_val = 0;
func<int (int)> n;// = func<int (int)>();
class A {
    public:
        A(int x) { cout << "default A() called" << endl; }
};
/// declaring A a() would declare a function even if A had 
/// a default constructor. However with an argument there
/// can't be any confusion with a function declaration.
A a(5);
int 
main () {
    //n();
    n(8);
    cout << "The x_val is " << x_val << endl;
    n = d;
    //f.s()
    //f(5);
    //n(8);
}
