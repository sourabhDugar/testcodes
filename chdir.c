#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>
#define EXIT_FAILURE 1

typedef void* (*fn)();

void
*readThread ()
{
    char *buf;
    char *start;
    int   i;
    FILE *flush;

    flush = fopen("flush.txt", "rwa");
    i = 0;
    buf = calloc(10, sizeof(char));
    start = buf;
    //free(buf);

    fprintf(stdout, "THREAD called %s\n", start);
    //memset(buf, 0, 10);
    while (strcmp("cd -", start) != 0) {
        fprintf(stderr, "%s", buf);
        fprintf(stderr, ":--:");
        memset(buf, 0, 10);
        read(1, buf, 4);
        buf[4] = '\0';
        //buf[++i] = getchar();
        //fprintf(stderr, "read char : %c\n", buf[i - 1]);
        //if (i >= 9) {
        //    break;
        //}
        //sleep(10);
    }

    return(NULL);
}

int
main ()
{
    int swd_fd;
    int ret;
    int r;
    int MAXSIZE = 0xfff;
    char proclnk[0xfff];
    char filename[0xfff];
    char new_dir[0xfff];
    swd_fd = open(".", O_RDONLY);

    if (swd_fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }
    sprintf(proclnk, "/proc/self/fd/%d", swd_fd);
    r = readlink(proclnk, filename, MAXSIZE);
    if (r > 0) {
        filename[r] = '\0';
    }
    fprintf(stdout, "The directory name is %s\n", filename);
    ret = close(swd_fd);
    sprintf(new_dir, "%s/../..", filename);
    swd_fd = open(new_dir, O_RDONLY);
    ret = chdir(new_dir);
    swd_fd = open(".", O_RDONLY);
    sprintf(proclnk, "/proc/self/fd/%d", swd_fd);
    r = readlink(proclnk, filename, MAXSIZE);
    if (r > 0) {
        filename[r] = '\0';
    } else {
		fprintf(stderr, "Can't get dirname\n");
	}

    fprintf(stdout, "The directory name is %s\n", filename);
    struct dirent *dir;
    DIR           *d;
    d = opendir(new_dir);
    if (d) {
        while ((dir = readdir(d)) != NULL) {
            fprintf(stdout, "dir : %s\n", dir->d_name);
        }
        closedir(d);
    }
    if (ret == -1) {
        perror("fchdir");
        exit (EXIT_FAILURE);
    }

    if (ret)
    {
        perror("close");
        exit (EXIT_FAILURE);
    }
    fn vtype = readThread;
    vtype();
    /*
    //while (1) {
        pthread_t tid;

        pthread_create(&tid, NULL, readThread, NULL);
        fprintf(stderr, "done with thread %ld\n", tid);

        pthread_join(tid, NULL);

        //fprintf(stderr, "done with thread %d\n", tid);
    //}
    */

}
