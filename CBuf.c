#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int rx_rate_history[20];
int tail_index;
int head_index;
int accum_intervals;

int
upd_rates (int val)
{
   		int                    rate = val;
        int                   *histrate = rx_rate_history;
        if (histrate) {
            *(histrate + tail_index) += rate;
            accum_intervals++;
            if (accum_intervals == 1) {
                tail_index = (tail_index + 1) % 20;
				/* reset the tail value */
                *(histrate + tail_index) = 0;
                if (tail_index == head_index) {
                	head_index = (head_index + 1) % 20;
				}
                accum_intervals = 0;
            }
        }
        return (1);
}


int
 main (int argc, char *argv[])
{
	int i = 0;
	for (i = 0; i < 45; i++) {
		upd_rates(i);
	}
	printf("Big number test\n");
	uint32_t b1 = 1922792;
	if (b1 & 1 << 31) {
		printf("Number is big enough\n");
	}
	if (((uint32_t)b1 * 8 * 1000) & (1 << 31)) {
		printf("Scaled Number is big enough\n");
	}
	uint32_t sB1 = ((unsigned long)b1 * 8 * 1000) / 10;
	printf("orig no : %u Division result : %u\n", b1, sB1);
	unsigned int sB2 = ((uint32_t)b1 * 8 * 1000) / 10;
	printf("Result of uncasted division : %u\n", sB2);
	printf("Sizeof unsigned long : %lu\n", sizeof(uint32_t));
}
