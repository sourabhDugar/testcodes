	.section	__TEXT,__text,regular,pure_instructions
	.globl	_tellMyEndianness
	.align	4, 0x90
_tellMyEndianness:
Leh_func_begin1:
	pushq	%rbp
Ltmp0:
	movq	%rsp, %rbp
Ltmp1:
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	movl	(%rax), %eax
	movl	%eax, -28(%rbp)
	movb	-28(%rbp), %al
	cmpb	$0, %al
	je	LBB1_2
	leaq	L_.str2(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	LBB1_3
LBB1_2:
	leaq	L_.str3(%rip), %rax
	movq	%rax, -24(%rbp)
LBB1_3:
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	popq	%rbp
	ret
Leh_func_end1:

	.globl	_thisIsHowMyStackGrows
	.align	4, 0x90
_thisIsHowMyStackGrows:
Leh_func_begin2:
	pushq	%rbp
Ltmp2:
	movq	%rsp, %rbp
Ltmp3:
	movq	%rdi, -8(%rbp)
	movq	-8(%rbp), %rax
	leaq	-25(%rbp), %rcx
	cmpq	%rax, %rcx
	jae	LBB2_2
	movq	_low_to_high(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	LBB2_3
LBB2_2:
	movq	_high_to_low(%rip), %rax
	movq	%rax, -24(%rbp)
LBB2_3:
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	popq	%rbp
	ret
Leh_func_end2:

	.globl	_thisIsHowMyHeapGrows
	.align	4, 0x90
_thisIsHowMyHeapGrows:
Leh_func_begin3:
	pushq	%rbp
Ltmp4:
	movq	%rsp, %rbp
Ltmp5:
	subq	$32, %rsp
Ltmp6:
	movq	%rdi, -8(%rbp)
	movabsq	$1, %rax
	movq	%rax, %rdi
	callq	_malloc
	movq	%rax, -32(%rbp)
	movq	-32(%rbp), %rax
	movq	-8(%rbp), %rcx
	cmpq	%rcx, %rax
	jae	LBB3_2
	movq	_high_to_low(%rip), %rax
	movq	%rax, -24(%rbp)
	jmp	LBB3_3
LBB3_2:
	movq	_low_to_high(%rip), %rax
	movq	%rax, -24(%rbp)
LBB3_3:
	movq	-24(%rbp), %rax
	movq	%rax, -16(%rbp)
	movq	-16(%rbp), %rax
	addq	$32, %rsp
	popq	%rbp
	ret
Leh_func_end3:

	.globl	_main
	.align	4, 0x90
_main:
Leh_func_begin4:
	pushq	%rbp
Ltmp7:
	movq	%rsp, %rbp
Ltmp8:
	subq	$64, %rsp
Ltmp9:
	movl	%edi, %eax
	movl	%eax, -4(%rbp)
	movq	%rsi, -16(%rbp)
	movl	$2, -36(%rbp)
	movl	$1, -40(%rbp)
	movl	-36(%rbp), %eax
	movb	%al, -41(%rbp)
	movabsq	$1, %rax
	movq	%rax, %rdi
	callq	_malloc
	movq	%rax, -56(%rbp)
	movb	-41(%rbp), %al
	cmpb	$2, %al
	jne	LBB4_2
	leaq	L_.str4(%rip), %rax
	movq	%rax, %rdi
	callq	_puts
	jmp	LBB4_3
LBB4_2:
	leaq	L_.str5(%rip), %rax
	movq	%rax, %rdi
	callq	_puts
LBB4_3:
	leaq	-40(%rbp), %rax
	movq	%rax, %rdi
	callq	_tellMyEndianness
	movq	%rax, %rcx
	xorb	%dl, %dl
	leaq	L_.str6(%rip), %rsi
	movq	%rsi, %rdi
	movq	%rcx, %rsi
	movb	%dl, %al
	callq	_printf
	movb	-36(%rbp), %al
	cmpb	$0, %al
	je	LBB4_5
	leaq	L_.str2(%rip), %rax
	movq	%rax, -32(%rbp)
	jmp	LBB4_6
LBB4_5:
	leaq	L_.str3(%rip), %rax
	movq	%rax, -32(%rbp)
LBB4_6:
	movq	-32(%rbp), %rax
	xorb	%cl, %cl
	leaq	L_.str7(%rip), %rdx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	movb	%cl, %al
	callq	_printf
	leaq	-36(%rbp), %rax
	movq	%rax, %rdi
	callq	_thisIsHowMyStackGrows
	xorb	%cl, %cl
	leaq	L_.str8(%rip), %rdx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	movb	%cl, %al
	callq	_printf
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	callq	_thisIsHowMyHeapGrows
	xorb	%cl, %cl
	leaq	L_.str9(%rip), %rdx
	movq	%rdx, %rdi
	movq	%rax, %rsi
	movb	%cl, %al
	callq	_printf
	movl	$1, -24(%rbp)
	movl	-24(%rbp), %eax
	movl	%eax, -20(%rbp)
	movl	-20(%rbp), %eax
	addq	$64, %rsp
	popq	%rbp
	ret
Leh_func_end4:

	.section	__DATA,__data
	.globl	_low_to_high
	.align	3
_low_to_high:
	.quad	L_.str

	.section	__TEXT,__cstring,cstring_literals
L_.str:
	.asciz	 "from high to low"

	.section	__DATA,__data
	.globl	_high_to_low
	.align	3
_high_to_low:
	.quad	L_.str1

	.section	__TEXT,__cstring,cstring_literals
L_.str1:
	.asciz	 "from low to high"

L_.str2:
	.asciz	 "little endian"

L_.str3:
	.asciz	 "big endian"

L_.str4:
	.asciz	 "expected dec"

L_.str5:
	.asciz	 "not expected"

L_.str6:
	.asciz	 "%s"

L_.str7:
	.asciz	 "I am %s.\n"

L_.str8:
	.asciz	 "My stack grows %s.\n"

L_.str9:
	.asciz	 "My heap grows %s.\n"

	.section	__TEXT,__eh_frame,coalesced,no_toc+strip_static_syms+live_support
EH_frame0:
Lsection_eh_frame:
Leh_frame_common:
Lset0 = Leh_frame_common_end-Leh_frame_common_begin
	.long	Lset0
Leh_frame_common_begin:
	.long	0
	.byte	1
	.asciz	 "zR"
	.byte	1
	.byte	120
	.byte	16
	.byte	1
	.byte	16
	.byte	12
	.byte	7
	.byte	8
	.byte	144
	.byte	1
	.align	3
Leh_frame_common_end:
	.globl	_tellMyEndianness.eh
_tellMyEndianness.eh:
Lset1 = Leh_frame_end1-Leh_frame_begin1
	.long	Lset1
Leh_frame_begin1:
Lset2 = Leh_frame_begin1-Leh_frame_common
	.long	Lset2
Ltmp10:
	.quad	Leh_func_begin1-Ltmp10
Lset3 = Leh_func_end1-Leh_func_begin1
	.quad	Lset3
	.byte	0
	.byte	4
Lset4 = Ltmp0-Leh_func_begin1
	.long	Lset4
	.byte	14
	.byte	16
	.byte	134
	.byte	2
	.byte	4
Lset5 = Ltmp1-Ltmp0
	.long	Lset5
	.byte	13
	.byte	6
	.align	3
Leh_frame_end1:

	.globl	_thisIsHowMyStackGrows.eh
_thisIsHowMyStackGrows.eh:
Lset6 = Leh_frame_end2-Leh_frame_begin2
	.long	Lset6
Leh_frame_begin2:
Lset7 = Leh_frame_begin2-Leh_frame_common
	.long	Lset7
Ltmp11:
	.quad	Leh_func_begin2-Ltmp11
Lset8 = Leh_func_end2-Leh_func_begin2
	.quad	Lset8
	.byte	0
	.byte	4
Lset9 = Ltmp2-Leh_func_begin2
	.long	Lset9
	.byte	14
	.byte	16
	.byte	134
	.byte	2
	.byte	4
Lset10 = Ltmp3-Ltmp2
	.long	Lset10
	.byte	13
	.byte	6
	.align	3
Leh_frame_end2:

	.globl	_thisIsHowMyHeapGrows.eh
_thisIsHowMyHeapGrows.eh:
Lset11 = Leh_frame_end3-Leh_frame_begin3
	.long	Lset11
Leh_frame_begin3:
Lset12 = Leh_frame_begin3-Leh_frame_common
	.long	Lset12
Ltmp12:
	.quad	Leh_func_begin3-Ltmp12
Lset13 = Leh_func_end3-Leh_func_begin3
	.quad	Lset13
	.byte	0
	.byte	4
Lset14 = Ltmp4-Leh_func_begin3
	.long	Lset14
	.byte	14
	.byte	16
	.byte	134
	.byte	2
	.byte	4
Lset15 = Ltmp5-Ltmp4
	.long	Lset15
	.byte	13
	.byte	6
	.align	3
Leh_frame_end3:

	.globl	_main.eh
_main.eh:
Lset16 = Leh_frame_end4-Leh_frame_begin4
	.long	Lset16
Leh_frame_begin4:
Lset17 = Leh_frame_begin4-Leh_frame_common
	.long	Lset17
Ltmp13:
	.quad	Leh_func_begin4-Ltmp13
Lset18 = Leh_func_end4-Leh_func_begin4
	.quad	Lset18
	.byte	0
	.byte	4
Lset19 = Ltmp7-Leh_func_begin4
	.long	Lset19
	.byte	14
	.byte	16
	.byte	134
	.byte	2
	.byte	4
Lset20 = Ltmp8-Ltmp7
	.long	Lset20
	.byte	13
	.byte	6
	.align	3
Leh_frame_end4:


.subsections_via_symbols
