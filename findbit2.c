#include <stdio.h>
#include <stdlib.h>

unsigned int
reversebits (unsigned int v)
{
	// swap odd and even bits
	v = ((v >> 1) & 0x55555555) | ((v & 0x55555555) << 1);
	// swap consecutive pairs
	v = ((v >> 2) & 0x33333333) | ((v & 0x33333333) << 2);
	// swap nibbles ...
	v = ((v >> 4) & 0x0F0F0F0F) | ((v & 0x0F0F0F0F) << 4);
	// swap bytes
	v = ((v >> 8) & 0x00FF00FF) | ((v & 0x00FF00FF) << 8);
	// swap 2-byte long pairs
	v = ( v >> 16             ) | ( v               << 16);
	return (v);
}

/*
 * 0th bit will be 0 here, and shift to obtain this will be 31
 * This is becoz 0 is the MSB and to obtain msb a 31 bit shift is needed..
 * However, input passed is 0, as msb is the 0th bit when viewing from left
 * to right...
 * we do a 31 - bit % 32 using ~bit & 1f to do the right conversion...
 * ~bit is same as 31 - bit (if bit is less than 31), but since it is more
 * than 6 bits, bits greater than 6 will be 1 due to ~, and hence we do an
 * AND with 0x1f(31) to filter out these bits...
 * @note
 *  SEE explanation in findbit.c
 */
int
main (int argc, char *argv[])
{
	char a[4]={0x9, 0x6, 0x1, 0x0};
	if (argc <= 1) {
		exit(1);
	}
	short us15 = 1<<15;
	unsigned int us15ext = (int)us15;
	int pos = atoi(argv[1]);
	int bit = pos;
	unsigned int *findin = (unsigned int *)a;
	//Need this for reversebits method
	//*findin = ntohl(*findin);
	bit = ~bit & 0x1f; //same as (31 - bit) % 32
	//comment the line below for reversebits printed result to work...
	bit = ~pos & 0x7;
	int ind = pos >> 3;
	printf("bit by 8 grouping bit%d:s:%d:%d, ind:%d, a[ind]:%d",
		   pos, bit, !!(a[ind] & (1 << bit)), ind, a[ind]);
	//for this method to work need ntohl on *findin
	//printf("bit%ds:%d:%d\n", pos, bit, !!(*findin & reversebits(1 << pos)));

}
