#include <iostream>
#include <map>
#include "assert.h"
using namespace std;
 
 
class BaseClass{
public:
virtual int funk() {
return 0;
}
};
 
class DerivedClassA : public BaseClass
{
private:
static short ID;
#ifndef ORIG
public:
#endif
virtual int funk() {
return 42;
}
 
public:
DerivedClassA(){};
virtual ~DerivedClassA(){};
};
 
class DerivedClassB : public BaseClass
{
private:
static short ID;
#ifndef ORIG
public:
#endif
virtual int funk() {
return 63;
}
 
public:
DerivedClassB(){};
virtual ~DerivedClassB(){};
};

#ifdef ORIG 
template<typename Type> BaseClass* createType() {
cout << "creating type \n";
return new Type;
}
#endif

class TemplatedFactory
{
public:
typedef BaseClass* (*ComponentFactoryFuncPtr)();
typedef map<const char*, ComponentFactoryFuncPtr> map_type;
 
map_type m_Map;
 
public:
#ifndef ORIG
template <typename S>
BaseClass *createType() 
{
    return new S;
}
#else
 
template<typename Type>
short AddType(const char* componentName){
ComponentFactoryFuncPtr function = &createType<Type>;
m_Map.insert(std::make_pair(componentName, function));
return 0;
};
#endif
};
 
 
 
 
int main(int argc, char ** argv)
{
 
TemplatedFactory * factory_p = new TemplatedFactory();
#ifdef ORIG
factory_p->AddType<DerivedClassA>("DerivedClassA");
factory_p->AddType<DerivedClassB>("DerivedClassB");
cout << "Done registering\n";

TemplatedFactory::map_type::iterator iterA = factory_p->m_Map.find("DerivedClassA");
TemplatedFactory::map_type::iterator iterB = factory_p->m_Map.find("DerivedClassB");
cout << "DerivedClassA: " << iterA->second()->funk() << "\n";
cout << "DerivedClassB: " << iterB->second()->funk() << "\n";
#else
DerivedClassA *dA = (DerivedClassA *)factory_p->createType<DerivedClassA>();
DerivedClassB *dB = (DerivedClassB *)factory_p->createType<DerivedClassB>();
cout << "DerivedClassA: " << dA->funk() << "\n";
cout << "DerivedClassB: " << dB->funk() << "\n";
assert (0 != 0 && "Dummy 0 is 0 ;)"); 
return 1;
#endif
return 0;
}
