#include <iostream>
using namespace std;


template <typename Sig>
class func {
    public:
    typedef Sig type;
    type        *f;
    ///Sig f();
    ///int (*f)(int) ;
    func (type d) : f (d) { }
    func () : f(0) { }
    int operator() (int x) { return f(x); }
    void operator= (type f_) { this->f = f_; }
};
template <typename t1, typename t2, typename t3>
class func<t1 (t2, t3)> {
    public:
    typedef t1 result_type;
    typedef t2 arg1;
    typedef t3 arg2;
    typedef t1 type(t2, t3);
    type *f;
    ///Sig f();
    ///int (*f)(int) ;
    func (type d) : f (d) { }
    func () : f(0) { }
    t1 operator() (t2 x, t3 y) { return f(x, y); }
    void operator= (type f_) { this->f = f_; }
};
#if 0
template <typename t1, typename t2>
class func1 : public func<t1 (t2)> {
    void operator= (typename func<t1 (t2)>::type f_) { this->f = f_; }
};
#endif
#if 0
template <typename Sig>
class func<int (int)> {
    public:
    typedef Sig sign;
    //typedef int sign(int) ;
    int (*orig_f) (int);
    int *s(int);
    func (sign in_f) : orig_f(in_f) {  } 
    func () :orig_f(0) { }
    int operator() (int x) { return orig_f(x); }
    void operator= (int (*x)(int)) {this->orig_f = x; }
};
#endif
class dfunc {
    public:
    int operator()(int x) { return 0; }
};
dfunc dfuncInst = dfunc();
int d (int x) { cout <<"d called "<< x << endl; return 0; }
int O (int x, int y) { cout <<"O called "<< x << endl; return 0; }
int(y) = 3;
/* what is int s(int) ???
typedef int s(int);
s S; //{return 1; };
*/

func<int (int)> f(d);
func<int (int, int)> n = O;

int 
main () {
    cout << "y " << y << endl;
func<int (int)> N;
    //f.s()
    f(5);
    n(8, 9);
}
