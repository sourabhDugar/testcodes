#include "treenode.h"
#include <iostream>
#include <string>
#include <queue>
    
using namespace sourabh;

string
serialize (const sourabh::Node *root)
{
    string serialTree;
    std::queue<const sourabh::Node *> childQ;

    childQ.push(root);
    childQ.push(Node::marker);

    while (!childQ.empty()) {
        const Node * iter = childQ.front();
        childQ.pop();
        
        if (iter == Node::marker) {
            serialTree += Node::marker->key;
            continue;
        }
        serialTree += iter->key;

        for (int i = 0, count = iter->getChildCount();
             i < count; ++i) {
            childQ.push(iter->child[i]);
        }
        childQ.push(Node::marker);
    }
    return serialTree;  
}

sourabh::Node *
deSerialize (const string serialTree)
{
    int serialLen = serialTree.length();
    std::queue<Node *> childQ;
    
    // The firt two elements should be root and marker keys..
    if (serialLen < 2) {
        return NULL;
    }
    
    // initialize root and parent
    Node * curParent = Node::newNode(serialTree[0]), * root = curParent;
    char markerKey = Node::marker->key; 

    // ignore marker key at position 1 and start at the 3rd element in the string
    for (int i = 2; i < serialLen; ++i) {
        Node * node;
        if (serialTree[i] != markerKey) {
            node = Node::newNode(serialTree[i]);
            childQ.push(node);
            curParent->addChild(node);
        } else {
            if (childQ.empty()) continue;
            curParent = childQ.front();
            childQ.pop();
        }
    }
    return root; 
}

int
main (int argc, char ** argv)
{
    sourabh::Node * root = sourabh::Node::newNode('R');
    Node * node0 = Node::newNode('0');
    Node * node1 = Node::newNode('1');    
    Node * node2 = Node::newNode('2');    
    Node * node3 = Node::newNode('3');    
    Node * node4 = Node::newNode('4');    
    Node * node5 = Node::newNode('5');    
    Node * node6 = Node::newNode('6');    
    root->addChild(node0);
    int childCount = root->addChild(node1);
    std::cout << "The child count is " << childCount << std::endl;
    node0->addChild(node2);
    node0->addChild(node3);
    node1->addChild(node4); 
    node2->addChild(node5); 
    node2->addChild(node6); 
    std::cout << "The serial string for tree is: " << serialize(root) << std::endl; 
    const Node * deserializedTree = deSerialize(serialize(root));
    std::cout << "Verifying serialize again: " << serialize(deserializedTree) << std::endl;
    return 0;  
}
