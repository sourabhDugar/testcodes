#include <iostream>
#include <tuple>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

#define maxHeight(a, b)  (((a) > (b)) ? (a) : (b))
#define min(a, b) (((a) < (b)) ? (a) : (b))

namespace sourabh_tuple {
template<int... s> struct seq {
    seq() {
        cout << "type 1 seq init" << endl;
    }
};

template<int max, int... s> struct make_seq  : make_seq< max - 1, max - 1, s... > {
    make_seq() {
        cout << "type 1 make_seq init" << endl;
    }
};

template<int... s> struct make_seq<0, s...> {
      typedef seq<s...> type;
      make_seq() {
          cout << "type 2 make_seq init" << endl;
      }
};

template<int max> using MakeSeq = typename make_seq<max>::type;

template<typename Seq, typename... Args>
struct foo_helper {
    foo_helper() {
        cout << "type 1 foo_helper init" << endl;
    }
};

template<int x, typename Arg>
struct foo_storage {
      foo_storage() {
          cout << "foo_storage init" << endl;
      }
      Arg data;
};

template<int s0, int... s, typename A0, typename... Args>
struct foo_helper<seq<s0, s...>, A0, Args...>:
  foo_storage<s0, A0>,
  foo_helper<seq<s...>, Args...>
{
    foo_helper() {
        cout << "type 2 foo_helper init" << endl;
    }
};

template<int N, typename T>
T& get(foo_storage<N, T> & f) {
    return f.data;
}

void test_tuple_getter() {
    make_seq<0, 1, 2> mseq2;
    make_seq<1> mseq3;
    cout << "**********" << endl;
    MakeSeq<2> mseq4;
    make_seq<3> mk2;
    cout << "==========" << endl;
    foo_helper<MakeSeq<3>, int, int, float>  foo1;
    get<0>(foo1) = 1;
    get<1>(foo1) = 2;
    get<2>(foo1) = 3.5;
    cout << get<0>(foo1) << ", " << get<2>(foo1) << ";" << endl;
}
}

namespace sourabh
{
enum {
    left, height, right,
    infinity = -1
};

enum overlap_type {
    overlap_extend, extend_only, overlap_only
};

ostream&
operator<<(std::ostream& strm, vector< tuple<int, int, int> >& in) {
    std::for_each(in.begin(), in.end(), [&strm](tuple<int, int, int>& t) {
            strm << "[" << std::get<0>(t) << "," << std::get<1>(t) << "," << std::get<2>(t)  << "]";
        });
    return strm;        
}

ostream&
operator<<(std::ostream& strm, const  vector< tuple<int, int, int> >& in) {
    std::for_each(in.begin(), in.end(), [&strm](const tuple<int, int, int>& t) {
            strm << "[" << std::get<0>(t) << "," << std::get<1>(t) << "," << std::get<2>(t)  << "]";
        });
    return strm;        
}

typedef tuple<int, int, int> bld;

class skyline_creator {
private:
    const vector< tuple<int, int, int> > buildings;
    vector< tuple<int, int, int> > skyline;
    vector<bld>::iterator  p;
    
public:
    skyline_creator(const vector< tuple<int, int, int> > b) : buildings(b) {
        skyline.push_back(b[0]);
        cout << "skyline initialized with buildings: " << buildings <<  endl;
        cout << "initial skyline " << skyline << endl;
    }
    
    bool extends(bld b) {
        return (get<left>(b) >= get<right>(skyline.back())) ? true : false; 
    }

    bool tail_overlap(bld b) {
        return (get<right>(b) > get<right>(skyline.back())) ? true : false; 
    }
    
    bool tail_overlap(bld b, bld b2) {
        return (get<right>(b2) > get<left>(b2)) ? true : false;
    }

    overlap_type extract_overlap(const tuple<int, int, int>& b) {
        if (extends(b)) {
            skyline.push_back(b);
            return extend_only;
        } else if (tail_overlap(b)) {
            // update the skyline with the extending piece and return the rest as overlap
            skyline.push_back({get<right>(skyline.back()), get<height>(b), get<right>(b)});
            return overlap_extend;
        } else {
            return overlap_only;
        }
    }

    bool overlaps(bld& b1, bld& b2) {
        return (get<left>(b1) < get<right>(b2)) ? true : false;
    }

    bool within(bld& b, bld& t) {
        return (get<left>(b) >= get<left>(t)) ? true : false;
    }

    bld * merge_into_skyline(bld& b, vector<bld>::iterator curr_iter) {
        bld curr = *curr_iter;
        bld *next = nullptr;
        vector<bld>::iterator insert_at = skyline.erase(curr_iter);
        vector<bld> *add = new vector<bld>;
        vector<bld>::iterator it = add->begin();
        if (get<left>(curr) < get<left>(b)) {
            it = add->insert(it, {get<left>(curr), get<height>(curr), get<left>(b)});
            it = add->insert(++it, {get<left>(b), maxHeight(get<height>(curr), get<height>(b)), min(get<right>(curr), get<right>(b))});
        } else {
            it = add->insert(it, {get<left>(curr), maxHeight(get<height>(curr), get<height>(b)), min(get<right>(curr), get<right>(b))});
        }
        if (get<right>(b) < get<right>(curr)) {
            add->insert(++it, {get<right>(b), get<height>(curr), get<right>(curr)});
        } 
        if (get<left>(b) < get<left>(curr)) {
            next = new bld {get<left>(b), get<height>(b), get<left>(curr)};
        }
        p = skyline.insert(insert_at, add->begin(), add->end());
        delete add;
        if (p != skyline.begin()) {
            --p;
        } else {
            p = skyline.end();
        }
        return next;    
    }


    void update_skyline(const bld& b, overlap_type ot) {
        if (ot == extend_only) return;
        bld *tmp = new bld {b};
        this->p = --skyline.end();
        if (ot == overlap_extend) --this->p; // in overlap_extend case we add the extension piece as last element, let us ignore the extension and only work on overlap part
        while (this->p != skyline.end() && tmp != nullptr) {
            bld * tmp2 = merge_into_skyline(*tmp, this->p);
            delete tmp;
            tmp = tmp2;
            //cout << "first run skyline -> " << skyline << endl;
        }
        if (tmp != nullptr) delete tmp;
    }

    skyline_creator const * operator()() {
        for (vector<bld>::const_iterator  b = ++buildings.begin(); b != buildings.end(); b++) {
            overlap_type ot = extract_overlap(*b);
            //if (overlap != nullptr) cout << "overlap is " << *(new vector<bld> {*overlap}) << endl;
            //cout << "skyline is " << skyline << endl;
            update_skyline(*b, ot);
            print_skyline();
        }
        return this;
    }

    void print_skyline() const {
        cout << "skyline is : " << skyline << endl;
    }
};

}


int main(int argc, char **argv)
{
    vector< tuple<int, int, int> > buildings {{1, 11, 5}, {2, 6, 7}, {3, 13, 9},
                      {12, 7, 16}, {14, 3, 25}, {19, 18, 22},
                      {23, 13, 29}, {24, 4, 28}};
    sourabh::skyline_creator sky = sourabh::skyline_creator(buildings);
    {
        using namespace sourabh;
        cout << "intial building vector: " << buildings << endl;
    }
    sky()->print_skyline();
    cout << "hello, world!" << endl;
    cout << sky.extends({2, 6, 7}) << endl;
    cout << sky.extends({12, 7, 16}) << endl;
    return (0);
}
