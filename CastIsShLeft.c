#include <stdio.h>
#include <stdlib.h>
#include "x.h"
#include <string.h>
//#include "x.c"
//#include <stdio.h>
typedef struct testStruct {
	int a
	;
} ts;

uint16_t
_OSReadInt16(
    const volatile void               * base,
    uintptr_t                     byteOffset
)
{
    return *(volatile uint16_t *)((uintptr_t)base + byteOffset);
}

ts B;
unsigned short
myntohs (unsigned short input)
{

     short output = 1;
     char *in=(char*)&input;
     /*
      * Casting to a larger size is like a shift left in C
      */
     output = (((unsigned short)in[1])) |
               ((unsigned short)((unsigned short)in[0]) << 8);

     return output;
}

typedef unsigned short (*call_by_fn_pointer)(unsigned short);
call_by_fn_pointer cIsShL = myntohs;

//This tries to put a pointer val in long
//to return. Basically trying our returning a pointer
//to caller using a single pointer arg. Generally this
//is done using a double pointer arg.

void TestPointerReturn (void *retval)
{
	int *retint = malloc(sizeof(int));
	*retint = 5964;
	*(long *)retval = (long)retint;
}

int**
Test2DArray ()
{
	int **twod_array = 0;
	twod_array = malloc(10 * sizeof(int *));
	int i = 0;
	for (i = 0; i < 10; i++) {
		*(twod_array + i) = malloc(10 * sizeof(int));
	}
	*(*(twod_array + 9) + 9) = 5964;
	twod_array[8][8] = 8966;
	twod_array[9][10] = 9599;
	return (twod_array);
}

int
main () {
	//hello();
	//hello2();
    char a[2] = {0x01, 0x02};
	char const *p = a;
	unsigned short *pa = (unsigned short *)&a[1];
    if (a[0] == 0x01)
		if (a[1] == 0x02)
			printf("\nall conds met\n");
		else
			printf("a[1] is not 0x01");
	printf("orig : %d rev'd : %d\n", *(unsigned short *)a,
           cIsShL(*(unsigned short *)a), 10);
	unsigned short x = (unsigned short)pa;
	long getVal;
	TestPointerReturn(&getVal);
	printf("got retVal as : %d\n", *(long *)getVal);
	int **twod = Test2DArray();
	char A[16] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x0a, 0x0b};
	char endline[45] ;
	memset(endline, '-', (sizeof(endline) - 1)*sizeof(char));
	endline[sizeof(endline)-40] = '\0';
	fprintf(stdout, "%-100s, %-7s\n", endline, "Rate");
	endline[sizeof(endline)-40] = '-';
	endline[sizeof(endline)-35] = '\0';
	char rate_word[] = "Rate";
	int abc[2];
	int *ab = 2 + abc;
	fprintf(stdout, "%-100s, %7s\n", endline, "Rate");
	_OSReadInt16(a, 2);
}
