// An Iterative C++ program to do DFS traversal from
// a given source vertex. DFS(int s) traverses vertices
// reachable from s.
//#include<bits/stdc++.h>
#include <list>
#include <stack>
#include <queue>
#include <iostream>
#include "graphheader.h"
#include <memory>
using namespace std;


MY_NAMESPACE_BEGIN(sourabh)
// This class represents a directed graph using adjacency
// std::list representation

Graph::Graph(int V)
{
    this->V = V;
    adj = new std::list<int>[V];
}
 
void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s std::list.
}

void Graph::DFSUtil(int s, bool visited[], Iter *iter)
{
    // Create a std::stack for DFS
    std::stack<int> stack;
 
    // Mark the current node as visited and push it
    visited[s] = true;
    stack.push(s);
 
    // 'i' will be used to get all adjacent vertices
    // of a vertex
    std::list<int>::iterator i;
    while (!stack.empty())
    {
        // Pop a vertex from std::stack and print it
        s = stack.top();
        std::cout << s << " ";
        stack.pop();
 
        // Get all adjacent vertices of the popped vertex s
        // If a adjacent has not been visited, then mark it
        // visited and push it to the std::stack
        for (i = adj[s].begin(); i != adj[s].end(); ++i)
        {
            if (!visited[*i])
            {
                if (iter != NULL) (*iter)(s, *i);
                visited[*i] = true;
                stack.push(*i);
            }
        }
    }
}

// prints all vertices in DFS manner
//__attribute__ ((__visibility__("hidden")))
void Graph::DFS()
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;
 
    for (int i = 0; i < V; i++)
        if (!visited[i])
           DFSUtil(i, visited, NULL);
}
typedef int(*iter_destructor)(IterBegin *);
void Graph::DFS(unique_ptr<IterBegin, iter_destructor> b, Iter *iter)
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;
 
    for (int i = 0; i < V; i++)
        if (!visited[i]) {
           (*b)(i);
           DFSUtil(i, visited, iter);
        }
}
MY_NAMESPACE_END
#if 0
// Driver program to test methods of graph class
int main(int argc, char ** argv)
{
    //int * arr = new int[atoi(argv[1])];
    // Let us create a disconnected graph without
    // edges 0-2 and 0-3 in above graph
    sourabh::Graph g(5);  // Total 5 vertices in graph
    g.addEdge(1, 0);
    g.addEdge(2, 1);
    g.addEdge(3, 4);
    g.addEdge(4, 0);
 
    std::cout << "Following is Depth First Traversal\n";
    g.DFS();
    std::cout << std::endl;
    return 0;
}
#endif

