#include <iostream>
#include "stdio.h"
using namespace std;
namespace MIb {}
//using namespace MIb;

#ifdef ALL
    #define MULTI
    #define SINGLE 2
    #define ARRAY_TEST
    #define VIRT_INHERITANCE 1
    #define TEST_VIRT_DEST
    #define TEST_VIRT_DEST_ERR
#endif


class A {
    string name;

public:
    int    a;

    A() { cout << "Default constructor called for A" << endl; }
    A (int x) : a(x)
    {
        cout << "Value of A.a is " << a << endl;
    }

    virtual int func(int);
    #ifndef TEST_VIRT_DEST_ERR
    virtual
    #endif
     ~A () { cout << "Done with A" << endl; }
};

int
A::func(int zz)
{
    cout << "I will return zz" << endl;
    return (zz);
}

class B : public A {

public:
    int    a;

    B ()  { cout << "Default constructor for B" << endl; }

    B (A &Aclass)
    {
        //cout << "Value BA.a " << A.a << endl;
        this->a = Aclass.a;
        cout << "Copied A to B" << endl;
    }
    //B (A *Aptr) { }

    virtual int func(int);

    void
    small_a (B obj)
    {
        cout << "Value of small a is " << obj.a << endl;
    }

    ~B() { cout << "Done with B" << endl; }
};

int
B::func(int zz)
{
    cout << "I will return zz + 10" << endl;
    return (zz + 10);
}

class FP
{
public:
    static int  hello(int);
    struct      data;
    int         print(data *d);
    /*
     * Pointer to member function
     * Needs to be dereferenced using ->* operator
     */
    typedef int (FP::*func_t)(data *);

    typedef struct data {
        int i;
        func_t func; //PointerToMember function (PMF) member of the struct
                     //need to use (classInstance)->*func to call the function
    } data;

    /*
     * Static member of class FP
     * needs to be declared again outside
     * else space will not be allocated for the
     * variable causing linker failure
     */
    static  data d;
    int     (*normPtr)(int);

    /*
     * Example of a function call via
     * 1. Pointer to Member function deref
     * 2. Standard Pointer to function deref
     */
    void
    process ()
    {
        (this ->* (d.func))(&d);///note: PMF is called using classInstance ->* PMF(args...)
        (*normPtr)(10);
    }

    FP ()
    {
        d.i = 999;
        d.func = &FP::print; // FP::d.func = FP::print is in-valid;
                             // Note FP::d.func can only be assigned something like &FP::MemberFuncOfFP
                             // MemberFuncOfFp should have compatible signature as declared for this PMF
        normPtr = hello;   // &FP::hello is valid too or FP::hello
    }

};

FP::data FP::d; //like data d//allocates space for the static d in class FP

int
FP::print (data *d)
{
    static int temp;
    printf("%d\n", d->i);
    cout << "print called" << endl;
    return (0);
}

int
FP::hello (int x)
{
    cout << "hello\n";
    return (100);
}

#if VIRT_INHERITANCE
class zero
{
    public:
        int zero;
};

class one
{
    public:
        one () { cout << "Default One called" << endl; }
        one (int x) : A(x) { cout << "One with int arg called" << endl; }
        int A;
        int B;
};

class two : public zero, virtual public one
{
    public:
        two () { cout << "Default Two called" << endl; }
        int A;
        int B;
};

class three : virtual public one
{
    public:
        three () { cout << "Default Three called" << endl; }
        int A;
        int B;
};

class four : public three, two
{
    public:
        four () : one(10) { two::A = 10; cout << "Default Four called" << endl; }
        four (one &oneclass) { cout << "Copy const called" << endl; }
        void putPvtOut() { cout << this->two::A << "-------------" << endl; }
        int A;
        int B;
};
#endif

class ArrayTest
{
    public:
        ArrayTest () { cout << "Default constructor called" << endl; }
        int X;
};

#ifdef MULTI
class MI1
{
    public:
        void func () { cout << "called from MI1" << endl; }
};

namespace MIb
{
class MI2
{
    public:
        void func () { cout << "called from MI2" << endl; }
};
int a = 10;
int b = 20;
}

//using namespace MIb;
class MILeaf : public MI1, public MIb::MI2
{
    public:
        void
        funcLeaf ()
        {
            cout << "Calling Leaf" << endl;
            cout << "value of a is " << MIb::a << endl;
            cout << "value of b is " << MIb::b << endl;
            MIb::MI2::func();
        }

};
#endif
#ifdef VFUNC
class Vf {
	public:
		int m1;
		/*
		 //impossible constructor
		 //needs derived available b4 base
		Vf(iVf ivf) {
			cout << "inited with ivf" << endl;
		}
		*/
		Vf(int x) : m1(x) {
			cout << "Class Vf inited" << endl;
		}
		virtual int
		virF (int x) {
			cout << "Vf virF called" << endl;
			return (1);
		}

};

class iVf : public Vf
{
	int m1;
	public:
		iVf(int x, int y) : m1(x), Vf(y)
		{
			cout << "Vf m1 : " << Vf::m1 << endl
				 << "iVf m1 : "<< m1 << endl;
		}
		virtual int
		virF (int z) {
			cout << "iVf virtual function virF called" << endl;
			return (1);
		}
};

#endif

int
main ()
{
#ifdef VFUNC
	iVf ivfObj = iVf(2, 4);
	Vf  *vfivfObjP = &ivfObj;
    vfivfObjP->virF(4);
	ivfObj.virF(3);
	(static_cast<Vf >(ivfObj)).virF(9);
	((Vf *)(&ivfObj))->virF(8);
	ivfObj.Vf::virF(10);
	Vf vf = *vfivfObjP;
	iVf *ivfsc = static_cast<iVf *>((Vf *)(&ivfObj));
	ivfsc->virF(25);
	vf.virF(18);
	Vf savitch_vf(9);
	iVf savitch_ivf(2, 9);
	savitch_vf = savitch_ivf; //assign derived to base
	//savitch_ivf = static_cast<iVf>(savitch_vf);//Illegal!
												//down-cast -> cast to derived

	exit(0);
#endif
#ifdef MULTI
    MILeaf M;
    M.MI1::func();
    M.funcLeaf();
#endif

#if SINGLE == 2
{
    FP f;
    f.process();
    cout << "^^^^^~~~~~~^^^^^^^" << endl;
    A obj1(10);
    //B objb;
    B obj3;
    //obj1 of type A is assigned to obj2 of type B
    //need the copy constructor in B to be able to do this
    B obj2 = obj1;
    cout << "*************" << endl;
    A obja0 = obj2;
    cout << "*************" << endl;
    cout << "obja0.a " << ((B)obja0).a << endl;
    A *obja = &obj2;
    B *objb = (B *)obja;//*((B *)obja);
    cout << "obj1.a " << obj1.a << endl;
    obj2.small_a(obj1);
    cout << "obj2.a " << ((A *)objb)->a << endl;
    cout << "obja.a " << obja->a << endl;
    //cout << "obj2.a " << obj2.a << endl;
}
#endif

#ifdef VIRT_INHERITANCE
    four Fobj;
    Fobj.A = 4;
    Fobj.B = 5;
    Fobj.putPvtOut();
    three &Fobj3 = Fobj;
    one &Fobj1 = Fobj3;

    Fobj1.A = 1;
    Fobj1.B = 2;

#endif

#ifdef TEST_VIRT_DEST
    /*
     * When deleting ptrToB of type A*
     * the destructor of type B is called only if A has a virtual destructor
     * else only the destructor of A is called
     * as A *ptrToB = new B(); basically is B *b = new B(); A *a = (A*)b;
     * without virtual destructor the destructor wont be in the vptrTable and
     * hence B's destructor can't be called.
     * This behavior can be checked by removing the virtual keyword qualifying ~A()
     * and then compiling and running this code
     */
    B *ptrToB = new B();
    //delete ptrToB;
    A *a = (A *)ptrToB;
    delete a;
#endif

#ifdef ARRAY_TEST
    ArrayTest **arr;

    //declares an array of pointers (10 ArrayTest *'s in this case)
    arr = new ArrayTest*[10];
    typedef int (*fn)(void);
    fn x;
    x = (fn)(new fn[10]);

    for (int i = 0; i < 10; i++) {
        *(arr + i) =  new ArrayTest[10];
    }

    cout << "Address Start:" << arr << endl;
    cout << "Address:" << &arr[1][1] << endl;
#endif

#if SINGLE
    ArrayTest *ar1;
    ar1 = new ArrayTest[10];
#endif
}
