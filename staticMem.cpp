#include <iostream>
using namespace std;

int g() {
   cout << "In function g()" << endl;
   return 0;
}

int s() {
   cout << "In function s()" << endl;
   return 0;
}

class X {
   public:
      static int g() {
         cout << "In static member function X::g()" << endl;
         return 1;
      }
};

class Y: public X {
   public:
      static int i;
};

int Y::i = ::g();
int x = X::g() * g();
int main() { }
